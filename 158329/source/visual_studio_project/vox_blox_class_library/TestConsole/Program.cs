﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using vox_blox;

namespace TestConsole
{
    class Program
    {
        [STAThread]
        static void Main(string[] args)
        {

            Stopwatch watch = new Stopwatch();

            watch.Start();
            
            Grid3D voxelmap = VoxelMapBuilder.ConvertTxtToArray("cone_wire_1.txt");

            watch.Stop();

            Console.WriteLine("ConvertTxtToArray complete. Time elapsed: " + watch.Elapsed.TotalSeconds.ToString());

            voxelmap.PrintContents();

            /*
            watch = new Stopwatch();

            watch.Start();

            Vox2BloxAStarSearchTree solutionBuilder = new Vox2BloxAStarSearchTree(voxelmap, "blocktypes.voxblox", null, false);

            watch.Stop();

            Console.WriteLine("Vox2BloxAStarSearchTree complete. Time elapsed: " + watch.Elapsed.TotalSeconds.ToString());

            watch = new Stopwatch();

            watch.Start();

            List<Vox2BloxOutputNode>[] outputList = solutionBuilder.ConvertVoxelToBricks();

            watch.Stop();

            Console.WriteLine("ConvertVoxelToBricks complete. Time elapsed: " + watch.Elapsed.TotalSeconds.ToString());
            */

            // process complete


        }

    }
        
}
