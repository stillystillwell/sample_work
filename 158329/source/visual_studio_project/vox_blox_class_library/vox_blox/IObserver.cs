﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

namespace vox_blox
{
    public interface IObserver
    {
        void UpdateMessage(Message message);
    }
}
