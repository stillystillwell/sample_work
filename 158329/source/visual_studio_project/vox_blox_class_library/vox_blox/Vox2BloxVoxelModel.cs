﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace vox_blox
{
    public class Vox2BloxVoxelModel
    {
        //Layer properties.
        private static int demoRows = 5, demoColumns = 7, demoHeight = 3;
        private int _totalNoOfLayers;
        private int[] _totalNoOfVoxels;
        private int _layerRows, _layerColumns;
        private int[][][] _layerMask;

        public Vox2BloxVoxelModel(Grid3D voxelMap, bool debugging)
        {
            if (debugging)
            {
                DebuggingSetup();
            }
            else
            {
                //ReadBinVoxFile();

                _totalNoOfLayers = voxelMap.GetHeight();
                _layerColumns = voxelMap.GetWidth();
                _layerRows = voxelMap.GetDepth();

                Initialise3LayerArray();

                AssignGridToMask(voxelMap);

            }
        }

        public void Initialise3LayerArray()
        {
            _layerMask = new int[_totalNoOfLayers][][];
            for (int i = 0; i < _totalNoOfLayers; i++)
            {
                _layerMask[i] = new int[_layerRows][];
                for (int j = 0; j < _layerRows; j++)
                {
                    _layerMask[i][j] = new int[_layerColumns];
                }
            }
        }

        public int GetHeightOfModel()
        {
            return _totalNoOfLayers;
        }
        public int GetRowOfLayer() { return _layerRows; }
        public int GetColumnOfLayer() { return _layerColumns; }
        public int[][][] GetLayerMask() { return _layerMask; }
        public int GetTotalVoxelsOfLayer(int height) { return _totalNoOfVoxels[height]; }
        
        //Counts voxels for each layer and assigns the relavant dimensions to each layer
        public void CountVoxels()
        {
            _totalNoOfVoxels = new int[demoHeight];
            _totalNoOfLayers = _layerMask.Length;
            for (int i = 0; i < demoHeight; i++)
            {
                int count = 0;
                _layerRows = _layerMask[i].Length;
                for (int j = 0; j < demoRows; j++)
                {
                    _layerColumns = _layerMask[i][j].Length;
                    for (int k = 0; k < demoColumns; k++)
                    {
                        if (_layerMask[i][j][k] == -1) count++;
                    }
                }
                _totalNoOfVoxels[i] = count;
            }
        }

        private void AssignGridToMask(Grid3D voxelMap)
        {
            for (int y = 0; y < _totalNoOfLayers; y++)
            {
                for (int z = 0; z < _layerRows; z++)
                {
                    for (int x = 0; x < _layerColumns; x++)
                    {
                        int value = voxelMap.GetElement(y, z, x);

                        if (value == 1) value = -1;

                        _layerMask[y][z][x] = value;

                    }
                }
            }
        }

        public void ReadBinVoxFile()
        {
            //Open files
            string path = "voxels.txt";
            //Mask index
            int y = 0, x = 0, z = 0;

            //Reads and copies file information into List
            if (File.Exists(path))
            {
                // Create a file to write to.
                using (StreamReader sw = File.OpenText(path))
                {
                    string s; 
                    while ((s = sw.ReadLine()) != null)
                    {
                        //
                        if (s[0] == 'd')
                        {
                            char[] seperators = { ' ', '\t' };
                            string[] tempContainer = s.Split(seperators);

                            //Debug Lines
                            Console.WriteLine("Layers: " + tempContainer[2] + "|");
                            Console.WriteLine("Rows: " + tempContainer[1] + "|");
                            Console.WriteLine("Columns: " + tempContainer[3] + "|");

                            //Get dimensions
                            _totalNoOfLayers = Convert.ToInt32(tempContainer[2]);
                            _layerRows = Convert.ToInt32(tempContainer[1]);
                            _layerColumns = Convert.ToInt32(tempContainer[3]);

                            //Initialise the mask container
                            Initialise3LayerArray();
                            Console.WriteLine("initialised");
                        }
                        else if (s[0] != '#')
                        {
                            //Convert voxel info into mask
                            int index = 0;
                            while (index < s.Length)
                            {
                                if (s[index] == '0')
                                {
                                    _layerMask[y][z][x] = 0;
                                    x++;
                                }
                                if (s[index] == '1')
                                {
                                    _layerMask[y][z][x] = -1;
                                    x++;
                                }

                                index++;
                                if (x >= _layerColumns)
                                {
                                    x = 0;
                                    z++;
                                }
                                else if (z >= _layerRows)
                                {
                                    x = 0;
                                    z = 0;
                                    y++;
                                }
                            }
                        }
                    }
                }
            }
            else Console.WriteLine("File not found");
            Console.WriteLine("Finsiie");
        }

        //Fills in the values for the layer(s)
        public void BuildMask()
        {
            for (int i = 0; i < demoHeight; i++)
            {
                for (int j = 0; j < demoRows; j++)
                {
                    for (int k = 0; k < demoColumns; k++)
                    {
                        _layerMask[i][j][k] = -1;
                    }
                }
            }
        }

        
        //For testing only!
        //Builds a dummy mask for debugging
        public void DebuggingSetup()
        {
            _totalNoOfLayers = demoHeight;
            _layerRows = demoRows;
            _layerColumns = demoColumns;
            Initialise3LayerArray();

            for (int y = 0; y < _totalNoOfLayers; y++)
            {
                for (int z = 0; z < _layerRows; z++)
                {
                    for (int x = 0; x < _layerColumns; x++)
                    {
                        if ((x == 5) && (z == 4) && (y == 1))
                        {
                            _layerMask[y][z][x] = 0;
                        }
                        else if ((x == 5) && (z == 4) && (y == 0))
                        {
                            _layerMask[y][z][x] = 0;
                        }
                        else
                        {
                            _layerMask[y][z][x] = -1;
                        }
                    }
                }
            }
        }

        //Outputs the map itself (in its original state)
        public void PrintMap()
        {

            for (int i = 0; i < _totalNoOfLayers; i++)
            {
                Console.WriteLine("Layer " + i);
                Console.Write(Environment.NewLine);



                for (int j = 0; j < _layerRows; j++)
                {
                    Console.Write(j + " | ");
                    for (int k = 0; k < _layerColumns; k++)
                    {
                        if (_layerMask[i][j][k] != -1)
                        {
                            Console.Write("{0, 3} ", _layerMask[i][j][k]);
                        }
                        else
                        {
                            Console.Write("{0, 3} ", "+");
                        }
                    }
                    Console.Write(Environment.NewLine);
                }
            }
        }

        // DEBUG

        public string GetMapData()
        {
            string returnData = null;

            for (int i = 0; i < _totalNoOfLayers; i++)
            {
                returnData += "Layer " + i.ToString() + "\n";

                for (int j = 0; j < _layerRows; j++)
                {
                    returnData += j.ToString() + " | ";

                    for (int k = 0; k < _layerColumns; k++)
                    {
                        if (_layerMask[i][j][k] != -1)
                        {
                            returnData += _layerMask[i][j][k].ToString();
                        }
                        else
                        {
                            returnData += "+";
                        }
                    }
                    returnData += "\n";
                }
            }

            return returnData;
            
        }


    }
}
