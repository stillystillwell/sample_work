﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace vox_blox
{
    public class Vox2BloxConverterWrapper
    {
        private string _errorPath;
        private string _outputPath;

        private string _errorMessage;
        private string _outputMessage;

        private Vox2BloxAStarSearchTree searchTree;
        private List<Vox2BloxOutputNode>[] outputBlox;

        public Vox2BloxConverterWrapper()
        {
            _errorPath = "Error.txt";
            _outputPath = "Output.txt";

            _errorMessage = "";
            _outputMessage = "";


            bool debugging = false;

            Grid3D dummy = new Grid3D(0, 0, 0);

            searchTree = new Vox2BloxAStarSearchTree(dummy, null, null, debugging);
        }

        public void MainScript()
        {
            //Run conversion algorithm form Vox2BloxAStarSearchTree
            outputBlox = searchTree.ConvertVoxelToBricks();

            //Write output to files

        }

        public void WriteErrorMessage()
        {
            if (!(File.Exists(_errorPath)))
            {
                File.Create(_errorPath);
            }

            StreamWriter sw = new StreamWriter(_errorPath);
            sw.WriteLine(_errorMessage);
            sw.Close();
        }

        public void WriteOutput()
        {
            //Write output
        }

        public void PrintOutput()
        {
            if (outputBlox != null)
            {
                int i = 0;
                foreach (List<Vox2BloxOutputNode> layer in outputBlox)
                {
                    Console.WriteLine("Layer " + i);
                    foreach (Vox2BloxOutputNode node in layer)
                    {
                        if (node != null)
                        {
                            Console.WriteLine(node.GetBlockID() + " " + node.GetXPosition() + " " + node.GetYPosition() + " " + node.GetRotation());
                        }
                        else
                        {
                            Console.WriteLine("NULL");
                        }
                    }
                    i++;
                }
            }
            else
            {
                Console.WriteLine("List is empty");
            }
        }
    }
}
