﻿using System;
using System.Collections.Generic;
using System.Text;

namespace vox_blox
{
    public class Message
    {
        private string title;
        private string content;

        public Message(string title, string content)
        {
            this.title = title;
            this.content = content;
        }

        public string getTitle()
        {
            return this.title;
        }

        public string getContent()
        {
            return this.content;
        }

    }
}
