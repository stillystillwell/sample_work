﻿using System;
using System.Collections.Generic;
using System.Text;

namespace vox_blox
{
    public class Grid3D
    {
        private int _height;
        private int _width;
        private int _depth;
        private int[][][] _grid;    

        // CONSTRUCTORS

        public Grid3D(int height, int width, int depth)
        {
            // Blank Constructor
            _height = height;
            _width = width;
            _depth = depth;
            InitGrid();
        }

        public Grid3D(Grid3D grid)
        {
            // Copy Constructor
            _height = grid.GetHeight();
            _width = grid.GetWidth();
            _depth = grid.GetDepth();
            CopyGrid3D(grid);
        }

        public Grid3D(Grid3D grid, int height, int width, int depth)
        {
            // Resize Constructor
            // Creates a new Grid3D object, populated with either the values from the given Grid3D object, or 0 if the index is out of bounds
            _height = height;
            _width = width;
            _depth = depth;
            InitGrid();
            CopyGrid3D(grid);
        }

        // PUBLIC FUNCTIONS

        public int GetHeight()
        {
            // Returns the height of the grid
            return this._height;
        }

        public int GetWidth()
        {
            // Returns the width of the grid
            return this._width;
        }

        public int GetDepth()
        {
            // Returns the depth of the grid
            return this._depth;
        }

        public int GetElement(int h, int w, int d)
        {
            // Returns the value of the element at the requested position
            return this._grid[h][w][d];
        }

        public void SetElement(int h, int w, int d, int value)
        {
            try
            {
                // Sets the value of the element at the requested position
                this._grid[h][w][d] = value;
            }
            catch (Exception e)
            {

            }

        }

        public void SetAllElements(int value)
        {
            // Sets all the elements in the grid to the specified value
            for (int i = 0; i < _height; i++)
            {
                for (int j = 0; j < _width; j++)
                {
                    for (int k = 0; k < _depth; k++)
                    {
                        _grid[i][j][k] = value;
                    }

                }

            }

        }

        public int[][] GetLayer(int h)
        {
            // Returns a 2D array for the request layer
            return this._grid[h];
        }

        public void SetLayer(int layer, int[][] data)
        {
            // Copy the values from a 2D array into a layer of the 3D array
            for (int i = 0; i < data.Length; i++)
            {
                for (int j = 0; j < data[i].Length; j++)
                {
                    this._grid[layer][i][j] = data[i][j];
                }

            }

        }

        // PRIVATE FUNCTIONS

        private void InitGrid()
        {
            // Builds 3D array 
            // Sets all values to 0

            _grid = new int[_height][][];

            for (int i = 0; i < _height; i++)
            {
                _grid[i] = new int[_width][];

                for (int j = 0; j < _width; j++)
                {
                    _grid[i][j] = new int[_depth];

                    for (int k = 0; k < _depth; k++)
                    {
                        _grid[i][j][k] = 0;
                    }

                }

            }

        }

        private void CopyGrid3D(Grid3D grid)
        {
            // Builds 3D array
            // Populates with same values as the given Grid3D object

            _grid = new int[_height][][];

            for (int i = 0; i < _height; i++)
            {
                _grid[i] = new int[_width][];

                for (int j = 0; j < _width; j++)
                {
                    _grid[i][j] = new int[_depth];

                    for (int k = 0; k < _depth; k++)
                    {
                        try
                        {
                            _grid[i][j][k] = grid.GetElement(i, j, k);
                        } 
                        catch (IndexOutOfRangeException)
                        {
                            _grid[i][j][k] = 0;
                        }
                                                
                    }

                }

            }

        }

        // PRINT METHODS

        public void PrintContents()
        {
            // Prints the contents for each layer of a Grid3D object
            Console.WriteLine("PRINTING GRID CONTENTS");

            for (int i = 0; i < _height; i++)
            {
                Console.WriteLine("Layer " + Convert.ToString(i) + ":");

                for (int j = _depth - 1; j >= 0; j--)       // Inverted loop, so output is visually correct (ie: lines are printed down screen)
                {
                    for (int k = 0; k < _width; k++)
                    {
                        Console.Write(Convert.ToString(this._grid[i][k][j]) + " ");
                    }
                    Console.Write("\n");
                }
                Console.Write("\n");
            }

        }

        public string ContentsToString()
        {
            string returnData = "Grid Contents: \n";

            for (int i = 0; i < _height; i++)
            {
                for (int j = 0; j < _width; j++)
                {
                    for (int k = 0; k < _depth; k++)
                    {
                        returnData += this._grid[i][j][k].ToString() + " ";
                    }
                    returnData += "\n";
                }
                returnData += "\n";
            }

            return returnData;

        }

    }

}
