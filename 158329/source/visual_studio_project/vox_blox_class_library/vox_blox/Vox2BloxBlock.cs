﻿using System;
using System.Collections.Generic;
using System.Text;

namespace vox_blox
{
    public class Vox2BloxBlock
    {

        /*Block class format: This class stores the relevant information required about each block within this program/script
         id - identifies the block logically 
         name - a name determined by the user (or default) for the user to see, for easier referencing. (Let's be honest, 1x1 block is easier to visualise than say 1234)
         
         _XLength, _YLength and _ZLength are all dimensions of the block as detemined by a 3-D plane, of the x-y-z axis.
            * Y axis the the height (vertical axis) of the block. We don't really give this much thought for the most part of the layer analysis unless we have a block of
              multiple layers.
            * We will deal with the X-Z plane for the most part in this format
              
          This   ^
          is     |1 1 1 1
          the    |1 1 1 1
          z-axis |1 1 1 1
                 |1 1 1 1
                 |1 1 1 1
                 |1 1 1 1
                 |_______________
                 
                    This is the 
                    x-axis

                * Order of traversal is usually left to right in an upward direction. (i.e. For each row, move from left to right) Important as this is 
                  how block mask data is stored!!!

            Block Mask is the block information (filled or empty spots in a 2-D plane) as stated above. It is stored in a 3 dimensional Jagged array.
                * Reason is because there are various points that we will need to travese 2D portions so we can't use a 3D mutli-dimensional array.
                * In addition, there may be portions that we need to individual look at each row so an array of 2D multidimensional arrays is also out.
                
            * Block Weight refers to the weight the block has (for heuritic purposes)
                * Currently, it is the total number of studs on the brick
                 
            * Rotation is the direction the block is orientated at.
                * Either 0, 90, 180, 270
         */

        //Variables are in the same order as they are in the file
        private int _blockID;
        private string _blockName;
        private int _XLength;
        private int _ZLength;
        private int _YLength;
        private int[][][] _blockMask;
        private int _blockWeight;
        private int _blockRotation;

        public Vox2BloxBlock(int blockID, string blockName, int z_length, int x_length, int y_length, int[][][] blockMap, int rotation)
        {
            //Initialiser
            _blockWeight = 0;

            //Load dimensions
            _blockID = blockID;
            _blockName = blockName;
            _XLength = x_length;
            _ZLength = z_length;
            _YLength = y_length;

            /*initialise block Mask and calculate block weight
            1.Create the array
            2.Copy the value from blockMap into _blockMap
            3. If that position is a 1, increment block weight
            */
            _blockMask = new int[y_length][][];
            for (int y = 0; y < y_length; y++)
            {
                _blockMask[y] = new int[z_length][];
                for (int z = 0; z < z_length; z++)
                {
                    _blockMask[y][z] = new int[x_length];
                    for (int x = 0; x < x_length; x++)
                    {
                        _blockMask[y][z][x] = blockMap[y][z][x];

                        //Incrments the block weight if there is a stud at that position
                        if (blockMap[y][z][x] == 1) _blockWeight++;
                    }
                }
            }

            //Load block rotation
            _blockRotation = rotation;
        }

        /*List of reutrn statements
         1. GetID - Returns the blockID
         2. GetBlockName - Returns the block name
         3. GetBlockXLength, GetBlockYLength, GetBlockZLength - Returns the dimension of the array
         4. GetBrickMask - Returns the mask information (generally used when checking if a brick can be placed in a position within the voxel model)
         5. GetBlockWeight - Returns the weightage of the block
         6. GetRotation - Return the rotation of the block
         
            Methods for debugging or out of use
            FillBlock was a method used to load the array but this was done in initialisation of the array itself so it may not be required in this context.
            (Could also be used to copy this information into an external array?)
            Print methods are for debugging
         */

        //Returns the brick id
        public int GetBlockID()
        {
            return _blockID;
        }

        //Returns the name of the block
        public string GetBlockName()
        {
            return _blockName;
        }

        //Returns the X-axis Length of the block
        public int GetBlockXLength()
        {
            return _XLength;
        }

        //Returns the Y-axis Length of the block
        public int GetBlockYLength()
        {
            return _YLength;
        }

        //Returns the Z-axis Length of the block
        public int GetBlockZLength()
        {
            return _ZLength;
        }

        //Returns the brick mask
        public int[][][] GetBlockMask()
        {
            return _blockMask;
        }

        //Returns the block's weight
        public int GetBlockWeight()
        {
            return _blockWeight;
        }

        //Returns the block's weight
        public int GetRotation()
        {
            return _blockRotation;
        }

        //Fills in the variable _blockMap with the block information
        public void FillBlocks(int[][][] blockMask)
        {
            for (int y = 0; y < _YLength; y++)
            {
                for (int z = 0; z < _ZLength; z++)
                {
                    for (int x = 0; x < _XLength; x++)
                    {
                        //copy information
                        _blockMask[y][z][x] = blockMask[y][z][x];
                    }
                }
            }
        }



        //Debugging print method
        public void PrintBrickInfo()
        {
            Console.Write("{0, 4} | {0, 10} | {1,6} | {2,5} | {3,6}", _blockID, _blockName, _XLength, _ZLength, _YLength);
            Console.Write(Environment.NewLine);
        }

        public void PrintBrickMask()
        {
            Console.WriteLine(_blockName);
            for (int y = 0; y < _YLength; y++)
            {
                Console.WriteLine("Level " + y);
                for (int z = 0; z < _ZLength; z++)
                {
                    for (int x = 0; x < _XLength; x++)
                    {
                        Console.Write(_blockMask[y][z][x] + " ");
                    }

                    //Leave line for next row
                    Console.Write(Environment.NewLine);
                }

                //Leave line for next layer
                Console.Write(Environment.NewLine);
            }
        }

        /*List of set statements
         1. SetID - Sets the blockID
         2. SetBlockName - Set the block name
         3. SetBlockXLength, SetBlockYLength, SetBlockZLength - Sets the dimension of the array
         4. SetBrickMask - Sets the mask information (generally used when checking if a brick can be placed in a position within the voxel model)
         5. SetBlockWeight - Sets the weightage of the block
         6. SetRotation - Sets the rotation of the block
         */
        public void SetBlockID(int blockID)
        {
            _blockID = blockID;
        }

        public void SetBlockName(string name)
        {
            _blockName = name;
        }

        public void SetBlockWeight(int weight)
        {
            _blockWeight = weight;
        }

        public bool SetBlockXLength(int length)
        {
            if (length >= 0)
            {
                _XLength = length;
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool SetYLength(int length)
        {
            if (length >= 0)
            {
                _YLength = length;
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool SetZLength(int length)
        {
            if (length >= 0)
            {
                _ZLength = length;
                return true;
            }
            else
            {
                return false;
            }
        }

        public string SetBrickMask(int[][][] blockMask)
        {
            //Initialise block dimensions
            int y_length = -1, z_length = -1, x_length = -1;
            string errorMessage = "";

            //Set values to blockMask dimensions
            try
            {
                y_length = blockMask.Length;
            }
            catch (OverflowException overflowEX)
            {
                errorMessage = errorMessage + "Overflow Exception thrown: Brick Mask Y-Axis is invalid" + Environment.NewLine;
            }
            try
            {
                z_length = blockMask[0].Length;
            }
            catch (OverflowException overflowEX)
            {
                errorMessage = errorMessage + "Overflow Exception thrown: Brick Mask Z-Axis is invalid" + Environment.NewLine;
            }
            try
            {
                x_length = blockMask[0][0].Length;
            }
            catch (OverflowException overflowEX)
            {
                errorMessage = errorMessage + "Overflow Exception thrown: Brick Mask X-Axis is invalid" + Environment.NewLine;
            }


            //Check if actual values are inserted
            if ((y_length != -1) && (z_length != -1) && (x_length != -1))
            {
                /*initialise block Mask and calculate block weight
                1.Create the array
                2.Copy the value from blockMap into _blockMap
                3. If that position is a 1, increment block weight
                */
                _blockMask = new int[y_length][][];
                for (int y = 0; y < y_length; y++)
                {
                    _blockMask[y] = new int[z_length][];
                    for (int z = 0; z < z_length; z++)
                    {
                        _blockMask[y][z] = new int[x_length];
                        for (int x = 0; x < x_length; x++)
                        {
                            _blockMask[y][z][x] = blockMask[y][z][x];

                            //Incrments the block weight if there is a stud at that position
                            if (blockMask[y][z][x] == 1) _blockWeight++;
                        }
                    }
                }
                errorMessage = "No errors. Block has been set";
            }
            return errorMessage;
        }

        public void SetRotation(int rotation)
        {
            _blockRotation = rotation;
        }
    }
}
