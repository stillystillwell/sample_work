﻿using System;
using System.Collections.Generic;
using System.Text;

namespace vox_blox
{
    class Cmd
    {
        private System.Diagnostics.Process thisProcess;


        public Cmd()
        {
            // instanciates the process
            thisProcess = new System.Diagnostics.Process();
        }

        public void CreateCmdProcess(string[] args)
        {
            // disallows console window to open
            thisProcess.StartInfo.CreateNoWindow = true;
            thisProcess.StartInfo.UseShellExecute = false;

            // sets initial directory
            thisProcess.StartInfo.FileName = "cmd.exe";
            thisProcess.StartInfo.Arguments = "/c " + string.Join(" ", args);
            Console.WriteLine(thisProcess.StartInfo.Arguments);
        }

        public void CreateCmdProcess(string[] args, string workingDirectory)
        {
            CreateCmdProcess(args);

            // adds working directory if specified
            thisProcess.StartInfo.WorkingDirectory = workingDirectory;

            Console.WriteLine(thisProcess.StartInfo.WorkingDirectory);
        }


        public void RunProcess()
        {
            // runs this instances process
            thisProcess.Start();
            thisProcess.WaitForExit();
        }
    }
}
