﻿using System;
using System.IO;
using System.Collections.Generic;

namespace vox_blox
{
    public static class ObjFileTransform
    {

        public static void TransformObjFile(string filepath, float qw, float qx, float qy, float qz)
        {
            // Read and store all of the relevant wavefront obj file data
            List<string> fileContents = ReadFileContents(filepath);

            // Apply transformations to captured data
            List<string> transformedData = new List<string>();
            if (fileContents != null) transformedData = TransformObjectData(fileContents, qw, qx, qy, qz);

            // Update file with transformed data
            if (transformedData != null) WriteToFile(filepath, transformedData);
        }

        private static List<string> ReadFileContents(string filepath)
        {
            // Reads the contents of a Wavefront .obj file
            // Returns a list of strings for the required data to be transformed/saved
            // Currently only captures the vertex (v) and face (f) data (as this is the only information required by Binvox)
            // https://en.wikipedia.org/wiki/Wavefront_.obj_file

            if (File.Exists(filepath))
            {
                List<string> returnData = new List<string>();

                try
                {
                    // Read each line and store as required
                    foreach (string line in File.ReadAllLines(filepath))
                    {
                        if (line.StartsWith("f ") || line.StartsWith("v ") || line.StartsWith("vn "))
                        {
                            returnData.Add(line);
                        }

                    }
                }
                catch (Exception e)
                {
                    // An error occured
                    Console.WriteLine(e.Message);
                }

                return returnData;

            }
            else {
                // File does not exist
                return null;
            }

        }

        private static List<string> TransformObjectData(List<string> data, float qw, float qx, float qy, float qz)
        {
            List<string> returnData = new List<string>();

            // Create the transformation matrix
            float[,] qMatrix = BuildQuaternionMatrix(qw, qx, qy, qz);
            float[,] iqMatrix = BuildInvertedQuaternionMatrix(qw, qx, qy, qz);

            // Parse each line of data and process accordingly
            foreach (string line in data)
            {
                if (line.StartsWith("v "))
                {
                    // A vertex has been found, transformation required
                    float[,] vMatrix = BuildVertexMatrix(line);
                    if (vMatrix != null)
                    {
                        // Apply rotation transformation to vertex data
                        string transformedData = ApplyVertexRotation(qMatrix, vMatrix, iqMatrix);
                        returnData.Add(transformedData);

                    }
                    else {
                        // There was an error parsing the vertex data
                        return null;
                    }

                }

                /*
                else if (line.StartsWith("vn "))
                {
                    // A vertex normal has been found, transformation required
                    float[] vnMatrix = BuildVerterxNormalMatrix(line);
                    if (vnMatrix != null)
                    {
                        // Apply rotation transformation to vertex data
                        string transformedData = ApplyVertexNormalRotation(vnMatrix, qw, qx, qy, qz);
                        returnData.Add(transformedData);

                    }
                    else {
                        // There was an error parsing the vertex data
                        return null;
                    }

                }
                */

                else if (line.StartsWith("f "))
                {
                    // A face has been found, no transformation required
                    returnData.Add(line);

                }
                else {
                    // Only 'v' and 'f' values should have been stripped from the .obj file
                    // This point should not be reached, but in case, this clause will prevent unrequired information being processed
                }

            }

            return returnData;
        }

        private static float[,] BuildQuaternionMatrix(float qw, float qx, float qy, float qz)
        {
            // Build quaternion matrix
            // http://what-when-how.com/advanced-methods-in-computer-graphics/quaternions-advanced-methods-in-computer-graphics-part-1/
            float[,] returnMatrix = new float[4, 4] {
                {  qw,  qx,  qy,  qz },
                { -qx,  qw, -qz,  qy },
                { -qy,  qz,  qw, -qx },
                { -qz, -qy,  qx,  qw }
            };

            return returnMatrix;
        }

        private static float[,] BuildInvertedQuaternionMatrix(float qw, float qx, float qy, float qz)
        {
            // Build conjugate matrix
            // http://what-when-how.com/advanced-methods-in-computer-graphics/quaternions-advanced-methods-in-computer-graphics-part-1/
            float[,] returnMatrix = new float[4, 4] {
                {  qw, -qx, -qy, -qz },
                {  qx,  qw, -qz,  qy },
                {  qy,  qz,  qw, -qx },
                {  qz, -qy,  qx,  qw }
            };

            return returnMatrix;
        }

        private static float[,] BuildVertexMatrix(string data)
        {
            // Takes a string containing vertex data and turn it into a vector
            string[] splitData = data.Split(' ');
            try
            {

                float[,] pointVector = new float[,] {
                    { (splitData.Length < 5) ? 1.0f : Convert.ToSingle(splitData[4]) }, // w = check if w value exists, if none default is 1.0
                    { Convert.ToSingle(splitData[1]) },                                 // x
                    { Convert.ToSingle(splitData[2]) },                                 // y
                    { Convert.ToSingle(splitData[3]) }                                  // z
                };
                return pointVector;

            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                return null;
            }

        }

        private static float[] BuildVerterxNormalMatrix(string data)
        {
            // Takes a string containing vertex normal data and turn it into a vector
            string[] splitData = data.Split(' ');
            try
            {

                float[] pointVector = new float[] {
                    0.0f,                             // w - always 0.0
                    Convert.ToSingle(splitData[1]),     // x
                    Convert.ToSingle(splitData[2]),     // y
                    Convert.ToSingle(splitData[3])      // z
                };
                return pointVector;

            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                return null;
            }

        }

        private static string ApplyVertexRotation(float[,] qMatrix, float[,] vMatrix, float[,] iqMatrix)
        {
            // Sandwich Product
            // http://www.euclideanspace.com/maths/algebra/realNormedAlgebra/quaternions/transforms/index.htm
            // http://what-when-how.com/advanced-methods-in-computer-graphics/quaternions-advanced-methods-in-computer-graphics-part-1/

            float[,] transformedData = MultiplyMatrices(iqMatrix, (MultiplyMatrices(qMatrix, vMatrix)));

            try
            {

                string wVal = transformedData[0, 0].ToString();
                string xVal = transformedData[1, 0].ToString();
                string yVal = transformedData[2, 0].ToString();
                string zVal = transformedData[3, 0].ToString();

                string returnString = "v " + xVal + " " + yVal + " " + zVal + " " + wVal;

                Console.WriteLine(returnString);

                return returnString;

            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                return null;
            }

        }

        private static float[,] MultiplyMatrices(float[,] matrixA, float[,] matrixB)
        {
            // http://stackoverflow.com/a/20800090

            int rA = matrixA.GetLength(0);      // # rows in matrixA
            int cA = matrixA.GetLength(1);      // # columns in matrixA
            int rB = matrixB.GetLength(0);      // # rows in matrixB
            int cB = matrixB.GetLength(1);      // # columns in matrixB

            if (cA != rB)   // Matrices cannot be multiplied
            {
                return null;

            }
            else {

                float[,] resultMatrix = new float[rA, cB];

                for (int i = 0; i < rA; i++)                            // Outer loop to iterate through rows of matrixA
                {
                    for (int j = 0; j < cB; j++)                        // Nested loop to iterate through columns of matrixB
                    {
                        float sum = 0.0f;                               // Keeps track of product of matrix multiplication

                        for (int k = 0; k < cA; k++)                    // Another nested loop to iterate through columns of matrixA
                        {
                            sum += matrixA[i, k] * matrixB[k, j];       // Apply multiplication of matrix cells
                        }

                        resultMatrix[i, j] = sum;                       // Add result to table
                    }

                }

                return resultMatrix;

            }

        }

        private static string ApplyVertexNormalRotation(float[] vnMatrix, float qw, float qx, float qy, float qz)
        {
            // Hamilton Product
            // https://en.wikipedia.org/wiki/Quaternion#Hamilton_product
            // http://math.stackexchange.com/a/535223
            // https://au.mathworks.com/help/aerotbx/ug/quatmultiply.html

            try
            {
                float[] transformedData = CalculateHamilton(CalculateHamilton(vnMatrix, qw, qx, qy, qz), qw, -qx, -qy, -qz);

                string wVal = transformedData[0].ToString();
                string xVal = transformedData[1].ToString();
                string yVal = transformedData[2].ToString();
                string zVal = transformedData[3].ToString();

                string returnString = "vn " + xVal + " " + yVal + " " + zVal + " " + wVal;
                //string returnString = "vn " + xVal + " " + yVal + " " + zVal;

                Console.WriteLine(returnString);

                return returnString;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                return null;
            }

        }

        private static float[] CalculateHamilton(float[] vn, float qw, float qx, float qy, float qz)
        {
            float[] returnData = new float[] {
                (qw * vn[0] - qx * vn[1] - qy * vn[2] - qz * vn[3]),
                (qw * vn[1] - qx * vn[0] - qy * vn[3] - qz * vn[2]),
                (qw * vn[2] - qx * vn[3] - qy * vn[0] - qz * vn[1]),
                (qw * vn[3] - qx * vn[2] - qy * vn[1] - qz * vn[0])
            };

            return returnData;
        }

        private static void WriteToFile(string filepath, List<string> data)
        {
            using (StreamWriter sw = new StreamWriter(filepath))
            {
                foreach (string s in data) sw.WriteLine(s);
            }
        }
        
    }

}
