﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Windows.Forms;

namespace vox_blox
{
    public class FileIO : ISubject
    {
        private List<IObserver> _observers = new List<IObserver>();
        private Message _message;

        // CONSTRUCTORS

        public FileIO()
        {
            
        }

        public FileIO(IObserver o)
        {
            if (o != null) RegisterObserver(o);
        }

        // INTERFACE IMPLEMENTATION

        public void RegisterObserver(IObserver o)
        {
            _observers.Add(o);
        }

        public void RemoveObserver(IObserver o)
        {
            _observers.Remove(o);
        }

        public void NotifyObservers()
        {
            foreach (var obs in _observers)
            {
                obs.UpdateMessage(_message);
            }
        }

        private void CreateMessage(string title, string content)
        {
            if (_observers.Count != 0)
            {
                _message = new Message(title, content);
                NotifyObservers();
            }
        }

        // FILE INPUT/OUTPUT METHODS

        public string GetExternalFilePath()
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            FileInfo fileInfo = null;

            openFileDialog.Filter = "3D object files (*.obj)| *.obj";

            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                fileInfo = new FileInfo(openFileDialog.FileName);
            }

            if (fileInfo != null)
            {
                return fileInfo.FullName;
            }
            else
            {
                return null;
            }
                
        }

        public string GetProjectSaveFilePath()
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            FileInfo fileInfo = null;

            saveFileDialog.Filter = "voxblox project files (*.voxblox)|*.voxblox";

            if(saveFileDialog.ShowDialog() == DialogResult.OK)
            {
                fileInfo = new FileInfo(saveFileDialog.FileName);
            }

            if (fileInfo != null)
            {
                return fileInfo.FullName;
            }
            else
            {
                return null;
            }

        }

        public string GetProjectOpenFilePath()
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            FileInfo fileInfo = null;

            openFileDialog.Filter = "voxblox project files (*.voxblox)|*.voxblox";

            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                fileInfo = new FileInfo(openFileDialog.FileName);
            }

            if (fileInfo != null)
            {
                return fileInfo.FullName;
            }
            else
            {
                return null;
            }
        }

        public string GetProjectExportFilePath()
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            FileInfo fileInfo = null;

            saveFileDialog.Filter = "Plain Text File (*.txt)|*.txt";

            if (saveFileDialog.ShowDialog() == DialogResult.OK)
            {
                fileInfo = new FileInfo(saveFileDialog.FileName);
            }

            if (fileInfo != null)
            {
                return fileInfo.FullName;
            }
            else
            {
                return null;
            }

        }

        public FileInfo GetObjFileInfo()
        {
            try
            {
                FileInfo fileInfo = new FileInfo(GetExternalFilePath());
                return fileInfo;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public void MoveFile(string from, string to)
        {
            CreateMessage("MoveFile", "Moving file");
            Cmd cmd = new Cmd();

            cmd.CreateCmdProcess(new string[] { "MOVE", "/Y", from, to });
            cmd.RunProcess();
            CreateMessage("MoveFile", "File move complete");
        }

        public void CopyFile(string from, string to)
        {
            CreateMessage("CopyFile", "Copying file");
            Cmd cmd = new Cmd();

            cmd.CreateCmdProcess(new string[] { "COPY", "/Y", from, to });
            cmd.RunProcess();
            CreateMessage("CopyFile", "File copy complete");
        }

        public string UpDir(string filepath, int updir)
        {
            for(int i = 0; i < updir; i++)
                filepath = Path.GetFullPath(Path.Combine(filepath, @"..\"));

            return filepath; 
        }

        public string DwnDir(string filepath, string[] folders)
        {
            filepath = Path.GetFullPath(Path.Combine(filepath, String.Join(@"\", folders)));
            return filepath;
        }

        public void DeleteAllFiles(string directory)
        {
            // Deletes all files in the specified folder and within any subdirectories (but not the subdirectory folder)
            Cmd cmd = new Cmd();
            cmd.CreateCmdProcess(new string[] {
                "del",                  // delete
                directory + "\\*",      // from within this directory, all files
                "/s",                   // and all subdirectories
                "/q",                   // without querying for confirmation
                "/f"                    // and force delete any read-only files
            });
            cmd.RunProcess();
        }

    }

}
