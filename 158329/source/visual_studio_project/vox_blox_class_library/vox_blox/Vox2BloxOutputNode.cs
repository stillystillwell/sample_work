﻿using System;
using System.Collections.Generic;
using System.Text;

namespace vox_blox
{
    public class Vox2BloxOutputNode
    {
        private int _blockID;
        private int _rotation;
        private int _xPosition;
        private int _zPosition;

        public Vox2BloxOutputNode(int blockID, int rotation, int xPosition, int zPosition)
        {
            _blockID = blockID;
            _rotation = rotation;
            _xPosition = xPosition;
            _zPosition = zPosition;
        }

        public int GetBlockID()
        {
            return _blockID;
        }

        public int GetXPosition()
        {
            return _xPosition;
        }

        public int GetYPosition()
        {
            return _zPosition;
        }

        public int GetRotation()
        {
            return _rotation;
        }

        public void SetBlockID(int blockID)
        {
            _blockID = blockID;
        }

        public void SetPosition(int xPosition, int zPosition)
        {
            _xPosition = xPosition;
            _zPosition = zPosition;
        }

        public void SetRotation(int rotation)
        {
            _rotation = rotation;
        }
    }
}