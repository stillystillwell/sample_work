﻿using System;
using System.Collections.Generic;
using System.Text;

namespace vox_blox
{
    class Vox2BloxSearchTreeNode
    {

        private int[][] _STreeNode_Layer;                            //Shows the current layer (empty and filled slots)
        private int _STreeNode_HeuristicValue;                       //Carries the heuristic value of the current Node
        private int _currentBrick;                                   //Index for the current brick
        private int _currentBrickWeight, _currentRow, _currentColumn;  //Holds the values for the pointer
        private int _noOfBricks;                                     //No of bricks in the layer
        private int _totalConnections;                               //The total connections for this layer on the one below
        private List<Vox2BloxOutputNode> _outputBlox;                     //Holds the list of outputnodes to send to the unity program

        //Takes the layer info and the h_value of the newest brick
        public Vox2BloxSearchTreeNode(ref int[][] layer, int row, int column, int totalNoOfBricks, int currentBrickIndex, int totalBrickWeight, int newBrickWeight, int totalConnectionsMade, List<Vox2BloxOutputNode> outputBlox)
        {
            //Initialisers
            _STreeNode_Layer = layer;
            _currentBrick = currentBrickIndex;
            _currentBrickWeight = totalBrickWeight;
            _STreeNode_HeuristicValue = 0;
            _currentRow = row;
            _currentColumn = column;
            _noOfBricks = totalNoOfBricks;
            _totalConnections = totalConnectionsMade;

            //Distance + Brick Weight + Connections made - No of Bricks Used
            //No of Bricks used will simple minus 1 of the current h_value
            _STreeNode_HeuristicValue = DistanceCal() + TotalBrickWeightCal(newBrickWeight) + (totalConnectionsMade * 10000) - _noOfBricks;
            SetOutPutBlox(outputBlox);
        }

        public Vox2BloxSearchTreeNode(ref int[][] layer, int row, int column, int totalNoOfBricks, int currentBrickIndex, int totalBrickWeight, int newBrickWeight, int totalConnectionsMade, ref List<Vox2BloxOutputNode> outputBlox)
        {
            //Initialisers
            _STreeNode_Layer = layer;
            _currentBrick = currentBrickIndex;
            _currentBrickWeight = totalBrickWeight;
            _STreeNode_HeuristicValue = 0;
            _currentRow = row;
            _currentColumn = column;
            _noOfBricks = totalNoOfBricks;
            _totalConnections = totalConnectionsMade;

            //Distance + Brick Weight + Connections made - No of Bricks Used
            //No of Bricks used will simple minus 1 of the current h_value
            _STreeNode_HeuristicValue = DistanceCal() + TotalBrickWeightCal(newBrickWeight) + (totalConnectionsMade * 10000) - _noOfBricks;
            SetOutPutBlox(outputBlox);
        }

        //Calculate Brick Weight - No of bricks used
        public int TotalBrickWeightCal(int newWeight)
        {
            _currentBrickWeight = _currentBrickWeight + newWeight;
            return _currentBrickWeight;
        }

        public int DistanceCal()
        {
            int distance = 0;
            for (int i = 0; i < _STreeNode_Layer.Length; i++)
            {
                for (int j = 0; j < _STreeNode_Layer[i].Length; j++)
                {
                    if (_STreeNode_Layer[i][j] != -1) distance++;

                }
            }
            return distance;
        }

        //Set methods
        public void SetOutPutBlox(List<Vox2BloxOutputNode> outputBlox)
        {
            if (outputBlox == null)
            {
                _outputBlox = new List<Vox2BloxOutputNode>();
            }
            else
            {
                _outputBlox = new List<Vox2BloxOutputNode>(outputBlox);
            }
        }

        //Get methods
        public int GetCurrentRow() { return _currentRow; }
        public int GetCurrentColumn() { return _currentColumn; }

        public int[][] GetArray()
        {
            return _STreeNode_Layer;
        }

        public int GetHValue()
        {
            return _STreeNode_HeuristicValue;
        }

        public int GetCurrentBrickValue() { return _currentBrick; }
        public int GetTotalBrickWeight() { return _currentBrickWeight; }
        public int GetNoOfBricks() { return _noOfBricks; }
        public int GetTotalConnections() { return _totalConnections; }

        public List<Vox2BloxOutputNode> GetListOfBricks() { return _outputBlox; }
    }
}
