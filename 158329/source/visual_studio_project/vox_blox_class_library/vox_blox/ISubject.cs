﻿using System;
using System.Collections.Generic;
using System.Text;

namespace vox_blox
{
    public interface ISubject
    {
        void RegisterObserver(IObserver o);
        void RemoveObserver(IObserver o);
        void NotifyObservers();
    }
}
