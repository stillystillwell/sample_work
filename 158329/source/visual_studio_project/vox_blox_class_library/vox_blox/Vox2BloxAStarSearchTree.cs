﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace vox_blox
{
    public class Vox2BloxAStarSearchTree : ISubject
    {
        //Model information
        /* All the properties of the voxelised model that must be imported.
         * totalHeight is the number of layers
         * totalRow and total Columns are relative to their current layer. Hence they are in arrays.
            * While in reality we require all information from the original model (including empty spaces), which means that we can do without an array, 
              the array is just a safety concern in case the model information does not comply as such.
         */
        private Vox2BloxVoxelModel _voxelisedModel;

        private int _modelHeight;
        private int _modelDepth;
        private int _modelWidth;

        //This holds the final masks for each layer
        private int[][][] _convertedModelMask;

        //List of legal blocks (including their orientations)
        private List<Vox2BloxBlock> _legalBlocks;

        //The search tree
        private List<Vox2BloxSearchTreeNode> _searchTree;

        //List of expanded masks --- May not be used due to the need for less optimal arrangements of blocks with better connectivity needed!
        private List<int[][]> _expandedList;

        //Constructor
        public Vox2BloxAStarSearchTree(Grid3D voxelMap, string blockDataFile, IObserver o, bool debugging)
        {

            RegisterObserver(o);    // Message passing interface initialization
            
            // Create the data required for the legal blocks
            InitializeBlockData(blockDataFile);

            // Convert the Grid3D voxelmap into a Vox2BloxVoxelModel voxelmap
            // TODO: replace with Grid3D
            _voxelisedModel = new Vox2BloxVoxelModel(voxelMap, debugging);
            
            // Initialize dimensions
            _modelHeight = _voxelisedModel.GetHeightOfModel();
            _modelDepth  = _voxelisedModel.GetRowOfLayer();
            _modelWidth  = _voxelisedModel.GetColumnOfLayer();

            // Initialises the model that holds the finalised information
            // TODO: replace with Grid3D
            _convertedModelMask = new int[_modelHeight][][];

            for (int i = 0; i < _modelHeight; i++)
            {
                _convertedModelMask[i] = new int[_modelDepth][];

                for (int j = 0; j < _modelDepth; j++)
                {
                    _convertedModelMask[i][j] = new int[_modelWidth];
                }

            }

            //Initialises list that holds searchTree
            _searchTree = new List<Vox2BloxSearchTreeNode>();
            
            //Initialise expandedList
            _expandedList = new List<int[][]>();
            
        }

        //Extract block information from file
        //Also finds the different orientations available
        private void InitializeBlockData(string filepath)
        {
            _legalBlocks = new List<Vox2BloxBlock>();
            
            // Find file containing block map data - located in the ProgramDirectory\Assets\Resources folder
            // string path = "Assets\\Resources\\blocktypes.voxblox";

            //Reads and copies file information into List
            if (File.Exists(filepath))
            {
                // Create a file to write to.
                using (StreamReader sw = File.OpenText(filepath))
                {
                    
                    //Holds 1 line of content from the file
                    string s = "";

                    //Clears First line
                    s = sw.ReadLine();

                    //Prepare reading block content into a file
                    int index = 0;
                    while ((s = sw.ReadLine()) != null)
                    {
                        //Container variables
                        int blockID = 0;
                        string blockName = "";
                        int blockXLength = 0;
                        int blockZLength = 0;
                        int blockYLength = 0;

                        //Copy block ID
                        while (s[index] != ' ')
                        {
                            blockID = (blockID * 10) + (s[index] - 48);
                            index++;
                        }

                        //Skip past current index (because it's a space)
                        index++;

                        //Copy block Name
                        while (s[index] != ' ')
                        {
                            blockName = blockName + s[index];
                            index++;
                        }

                        //Skip past current index (because it's a space)
                        index++;

                        //Copy current block z length
                        while ((s[index] >= 48) && (s[index] <= 57))
                        {
                            blockZLength = (blockZLength * 10) + (s[index] - 48);
                            index++;
                        }

                        //Skip past current index (because it's a space)
                        index++;

                        //Copy current block x length
                        while ((s[index] >= 48) && (s[index] <= 57))
                        {
                            blockXLength = (blockXLength * 10) + (s[index] - 48);
                            index++;
                        }

                        //Skip past current index (because it's a space)
                        index++;

                        //Copy current block y length
                        while ((s[index] >= 48) && (s[index] <= 57))
                        {
                            blockYLength = (blockYLength * 10) + (s[index] - 48);
                            index++;
                        }

                        //Skip past current index (because it's a space)
                        index++;

                        //Get brick data
                        int[][][] tempBlockMap;
                        //initialise temp block
                        tempBlockMap = new int[blockYLength][][];
                        for (int y = 0; y < blockYLength; y++)
                        {
                            tempBlockMap[y] = new int[blockZLength][];
                            for (int z = 0; z < blockZLength; z++)
                            {
                                tempBlockMap[y][z] = new int[blockXLength];
                                for (int x = 0; x < blockXLength; x++)
                                {
                                    tempBlockMap[y][z][x] = 0;
                                }
                            }
                        }

                        //Copy block information
                        int xIndex = 0, yIndex = 0, zIndex = 0;                        //Counters
                        while ((yIndex < blockYLength) && (index < s.Length))
                        {
                            while ((zIndex < blockZLength) && (index < s.Length))
                            {
                                while ((xIndex < blockXLength) && (index < s.Length))
                                {
                                    tempBlockMap[yIndex][zIndex][xIndex] = (s[index] - 48);
                                    index++;
                                    xIndex++;
                                }
                                zIndex++;
                                xIndex = 0;
                            }
                            yIndex++;
                            zIndex = 0;
                            xIndex = 0;
                        }
                        
                        //Builds new Brick
                        Vox2BloxBlock tempBlockContainer = new Vox2BloxBlock(blockID, blockName, blockZLength, blockXLength, blockYLength, tempBlockMap, 0);
                        //Add blocks to list
                        _legalBlocks.Add(tempBlockContainer);
                        
                        //Compare different orientation
                        List<int[][][]> tempBlockOrientation = new List<int[][][]>();
                        tempBlockOrientation.Add(tempBlockMap);
                        CheckOrientations(tempBlockOrientation, tempBlockMap, blockYLength, blockZLength, blockXLength, blockName, blockID);


                        //Reset index
                        index = 0;
                    }
                }
            }
            else
            {
                Console.WriteLine("File not found");
            }

        }

        //Checks the remain orientations of the current brick in clockwise direction
        private void CheckOrientations(List<int[][][]> tempBlockOrientation, int[][][] originalBlockMap, int yLength, int zLength, int xLength, string blockName, int blockID)
        {
            //Check 90 degrees
            //The XLength is now the ZLength (and vice versa)
            int[][][] tempBlockMap = new int[yLength][][];
            for (int y = 0; y < yLength; y++)
            {
                tempBlockMap[y] = new int[xLength][];
                for (int z = 0; z < xLength; z++)
                {
                    tempBlockMap[y][z] = new int[zLength];
                }
            }

            for (int y = 0; y < yLength; y++)
            {
                //Keep the counters the same for clarity purposes
                for (int z = 0; z < xLength; z++)
                {
                    for (int x = 0; x < zLength; x++)
                    {
                        tempBlockMap[y][z][x] = originalBlockMap[y][x][(xLength - 1) - z];
                    }
                }
            }

            //Check if new bloc exists (if zlength == xlength, the chances are that the block exists)
            if (!(CheckIfBlockExists(ref tempBlockOrientation, ref tempBlockMap, yLength, xLength, zLength)))
            {
                string tempBlockName = blockName + " (90)";
                //Builds new Brick
                Vox2BloxBlock tempBlockContainer = new Vox2BloxBlock(blockID, tempBlockName, xLength, zLength, yLength, tempBlockMap, 90);
                //Add blocks to list
                _legalBlocks.Add(tempBlockContainer);
                //Add current orientation
                tempBlockOrientation.Add(tempBlockMap);
            }

            //Check 180 degrees
            //xLength and zLength are returned to normal but now at position 00, we have the value of [zLength-1][xLength-1]
            tempBlockMap = new int[yLength][][];
            for (int y = 0; y < yLength; y++)
            {
                tempBlockMap[y] = new int[xLength][];
                for (int z = 0; z < zLength; z++)
                {
                    tempBlockMap[y][z] = new int[xLength];
                }
            }

            for (int y = 0; y < yLength; y++)
            {
                //Keep the counters the same for clarity purposes
                for (int z = 0; z < zLength; z++)
                {
                    for (int x = 0; x < xLength; x++)
                    {
                        tempBlockMap[y][z][x] = originalBlockMap[y][(zLength - 1) - z][(xLength - 1) - x];
                    }
                }
            }

            //Check if new bloc exists (if zlength == xlength, the chances are that the block exists)
            if (!(CheckIfBlockExists(ref tempBlockOrientation, ref tempBlockMap, yLength, zLength, xLength)))
            {
                string tempBlockName = blockName + " (180)";
                //Builds new Brick
                Vox2BloxBlock tempBlockContainer = new Vox2BloxBlock(blockID, tempBlockName, zLength, xLength, yLength, tempBlockMap, 180);
                //Add blocks to list
                _legalBlocks.Add(tempBlockContainer);
                //Add current orientation
                tempBlockOrientation.Add(tempBlockMap);
            }

            //Check 270 degrees
            //The XLength is now the ZLength (and vice versa)
            tempBlockMap = new int[yLength][][];
            for (int y = 0; y < yLength; y++)
            {
                tempBlockMap[y] = new int[xLength][];
                for (int z = 0; z < xLength; z++)
                {
                    tempBlockMap[y][z] = new int[zLength];
                }
            }

            for (int y = 0; y < yLength; y++)
            {
                //Keep the counters the same for clarity purposes
                for (int z = 0; z < xLength; z++)
                {
                    for (int x = 0; x < zLength; x++)
                    {
                        tempBlockMap[y][z][x] = originalBlockMap[y][(zLength - 1) - x][z];
                    }
                }
            }

            //Check if new bloc exists (if zlength == xlength, the chances are that the block exists)
            if (!(CheckIfBlockExists(ref tempBlockOrientation, ref tempBlockMap, yLength, xLength, zLength)))
            {
                string tempBlockName = blockName + " (270)";
                //Builds new Brick
                Vox2BloxBlock tempBlockContainer = new Vox2BloxBlock(blockID, tempBlockName, xLength, zLength, yLength, tempBlockMap, 270);
                //Add blocks to list
                _legalBlocks.Add(tempBlockContainer);
                //Add current orientation
                tempBlockOrientation.Add(tempBlockMap);
            }

        }

        public int GetDimension()
        {
            return _modelHeight;
        }

        public int[][][] GetMask()
        {
            return _voxelisedModel.GetLayerMask();
        }

        //Goes through each index in the map
        //If 2 slots are diferent, same is flagged to be false
        //If 2 slots are the same, same will remain true.
        //At the end of the checks, if same == true, block exists. Leave the method
        private bool CheckIfBlockExists(ref List<int[][][]> tempBlockOrientation, ref int[][][] tempBlockMap, int yLength, int zLength, int xLength)
        {
            bool same;                  //Used to check if a block is the same
            foreach (int[][][] map in tempBlockOrientation)
            {
                //Reset for every map
                same = true;

                //Check if the dimensions are the same.
                //if yes, enter the for loop sequence.
                if ((map.Length == yLength) && (map[0].Length == zLength) && (map[0][0].Length == xLength))
                {
                    for (int y = 0; y < yLength; y++)
                    {
                        for (int z = 0; z < zLength; z++)
                        {
                            for (int x = 0; x < xLength; x++)
                            {
                                if (map[y][z][x] != tempBlockMap[y][z][x])
                                {
                                    same = false;
                                }
                            }
                        }
                    }
                }
                else
                {
                    //Dimensions are different. Different blocks
                    same = false;
                }

                //Check if same is true. Yes; block exists. Return true
                if (same) return true;
            }

            //Left loop == block not found.
            return false;
        }

        //Adds new layer to the tree
        private void AddNewLayer(ref int[][] layer, int currentRow, int currentColumn, int totalNoOfBricks, int currentBrickIndex, int totalBrickWeight, int brickWeight, int totalConnectionsMade, List<Vox2BloxOutputNode> listOFBlox)
        {
            
            int[][] currentLayer = layer;

            //Expanded list info
            int[][] newLayer;
            int rows = currentLayer.Length;
            int columns = currentLayer[0].Length;

            //Used to test if an object already exists in the expanded list
            bool maskFound = false;
            
            Vox2BloxSearchTreeNode tempNode = new Vox2BloxSearchTreeNode(ref currentLayer, currentRow, currentColumn, totalNoOfBricks, currentBrickIndex, totalBrickWeight, brickWeight, totalConnectionsMade, listOFBlox);

            if (!(maskFound))
            {
                //Search and add by HValue
                if (_searchTree.Count == 0)
                {
                    _searchTree.Add(tempNode);
                }
                else
                {
                    //Sort according to hightest HValue. 
                    //If HVal is the same, add to the end of that series of the HVal
                    int index = 0;
                    while ((index < _searchTree.Count) && (tempNode.GetHValue() <= _searchTree[index].GetHValue()))
                    {
                        index++;
                    }

                    //Add to the right index
                    _searchTree.Insert(index, tempNode);
                }
            }
            else
            {
                Console.WriteLine("Mask matches current layer");
            }
        }

        private void AddNewLayer(ref int[][] layer, int currentRow, int currentColumn, int totalNoOfBricks, int currentBrickIndex, int totalBrickWeight, int brickWeight, int totalConnectionsMade, ref List<Vox2BloxOutputNode> listOFBlox)
        {
            int[][] currentLayer = layer;

            //Expanded list info
            int[][] newLayer;
            int rows = currentLayer.Length;
            int columns = currentLayer[0].Length;

            //Used to test if an object already exists in the expanded list
            bool maskFound = false;

            Vox2BloxSearchTreeNode tempNode = new Vox2BloxSearchTreeNode(ref currentLayer, currentRow, currentColumn, totalNoOfBricks, currentBrickIndex, totalBrickWeight, brickWeight, totalConnectionsMade, listOFBlox);

            if (!(maskFound))
            {
                //Search and add by HValue
                if (_searchTree.Count == 0)
                {
                    _searchTree.Add(tempNode);
                }
                else
                {
                    //Sort according to hightest HValue. 
                    //If HVal is the same, add to the end of that series of the HVal
                    int index = 0;
                    while ((index < _searchTree.Count) && (tempNode.GetHValue() <= _searchTree[index].GetHValue()))
                    {
                        index++;
                    }
                    //Add to the right index
                    _searchTree.Insert(index, tempNode);
                }
            }
            else
            {
                Console.WriteLine("Mask matches current layer");
            }
        }


        //Printing methods
        public void printFinalLayer()
        {
            int rows, column;
            rows = _modelDepth;
            column = _modelWidth;

            for (int h = 0; h < _modelHeight; h++)
            {
                Console.WriteLine("Layer " + h + ":");
                for (int i = 0; i < rows; i++)
                {
                    Console.Write(i + " | ");
                    for (int j = 0; j < column; j++)
                    {
                        if (_convertedModelMask[h][i][j] != -1)
                        {
                            Console.Write("{0, 3} ", _convertedModelMask[h][i][j]);
                        }
                        else
                        {
                            Console.Write("{0, 3} ", "+");
                        }
                    }
                    Console.Write(Environment.NewLine);
                }
                Console.Write(Environment.NewLine);
            }
        }

        private void printLayerViaNode(Vox2BloxSearchTreeNode currentNode, int layer)
        {
            int[][] layerMap = currentNode.GetArray();
            int currentRow = currentNode.GetCurrentRow(), currentColumn = currentNode.GetCurrentColumn();
            int rows, column;
            rows = _modelDepth;
            column = _modelWidth;

            //Console.WriteLine(rows + " " + column);
            for (int i = rows - 1; i >= 0; i--)
            {
                Console.Write(i + " | ");
                for (int j = 0; j < column; j++)
                {
                    if (layerMap[i][j] != -1)
                    {
                        Console.Write("{0, 3} ", layerMap[i][j]);
                    }
                    else
                    {
                        Console.Write("{0, 3} ", "+");
                    }
                }
                Console.Write(Environment.NewLine);
            }
            Console.Write(Environment.NewLine);
        }

        //Debugging method
        private bool PrintBricks()
        {
            if (_legalBlocks.Count == 0)
            {
                return false;
            }

            //Print brick info
            Console.WriteLine("Block Name | Length | Width | Height");
            foreach (Vox2BloxBlock brick in _legalBlocks)
            {
                brick.PrintBrickInfo();
                brick.PrintBrickMask();
            }
            return true;
        }


        //Conversion Algorithm
        public List<Vox2BloxOutputNode>[] ConvertVoxelToBricks()
        {
            /* Let's go through the conversion process
             * This method is meant to convert all available voxels into blocks
                * For each layer (i.e. each y value), extract the origrinal layerMask and load it into a TreeNode (and then into the tree) with the default values
                    * Default values include the origin of the pointer (most cases is 0,0). The no of bricks and connections will be 0 by default as well.
                * Traverse the tree until;
                    1. A solution is found
                    2. Or the tree is empty (Should not occur!!!)

                * This portion is covered under method BuildSolution
                    * Traverse each layer from left to right, along the z-axis.
                    * For each row (z-value), move through each position on the x-axis (the column).
                    * Check the layer mask against the block mask and see if a block can be placed.
                        * Add a new node to the tree for each possiblity
                
                * When a solution is found, 
                    1. We leave the search.
                    2. Add the layer Mask to the finalInformation array. 
                    2. Clear the searchTree list (and any other lists used in the loop).
                    
                * When all layers have been traverse, output relevant information.
                
            * This method is also encapsulated with a try/catch method.
                * Currently returns a true, if successful.
                * Returns a false if failed. (What to do in that case?)
             */

            //Build initial outputNode list
            List<Vox2BloxOutputNode>[] outputList = new List<Vox2BloxOutputNode>[_modelHeight];

            try
            {
                //Get the voxel model array from the model
                int[][][] maskCopy = _voxelisedModel.GetLayerMask();

                //Initialise the y-axis Index
                int currentHeight = 0;

                //Performs the conversion operation
                //Traverses layer by layer
                while (currentHeight < _modelHeight)
                {
                    ConvertLayer(ref outputList, maskCopy, currentHeight);
                    currentHeight++;
                }

                //Print out final layer
                //for reference only
                // printFinalLayer();

                return outputList;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return null;
            }

        }

        public void ConvertLayer(ref List<Vox2BloxOutputNode>[] outputList, int[][][] maskCopy, int currentHeight)
        {
            //layerComplete is a bool that "Checks if the layer is complete"
            /*
             * This is declared and defined here as it is required to be reset every time we start a new layer
             * If we hit the end of a layer, it is flagged true and we leave the loop.
             * Else it remains false and the loop continues .
             */
            bool layerComplete = false;

            //Add the unedited layer to the search tree
            outputList[currentHeight] = new List<Vox2BloxOutputNode>();
            if (currentHeight % 2 == 0)
            {
                List<Vox2BloxOutputNode> tempList = new List<Vox2BloxOutputNode>();
                AddNewLayer(ref maskCopy[currentHeight], 0, 0, 0, 1, 0, 0, 0, tempList);
            }
            else
            {
                List<Vox2BloxOutputNode> tempList = new List<Vox2BloxOutputNode>();
                AddNewLayer(ref maskCopy[currentHeight], _modelDepth - 1, _modelWidth - 1, 0, 1, 0, 0, 0, tempList);
            }

            //Tree has been built, currently storing 1 node (the starting node)

            //Pop the first node from the search Tree
            Vox2BloxSearchTreeNode currentNode = _searchTree[0];
            _searchTree.RemoveAt(0);

            //The above is the only instance of popping from the tree ever happening outside of the while loop

            //While current layer has no unconverted slots
            //And the layer is not complete (double negative ==  true)
            while ((AnyUnconvertedVoxels(ref currentNode, currentHeight)) && (!layerComplete))
            {
                //Call BuildSolution with the current node. BuildSolution will do the checks block by block

                if (currentHeight % 2 == 0)
                {
                    BuildSolutionLeft2Right(currentNode, ref currentHeight, ref layerComplete, ref outputList[currentHeight]);
                }
                else
                {
                    BuildSolutionRight2Left(currentNode, ref currentHeight, ref layerComplete, ref outputList[currentHeight]);
                }

                /*Check if we reached the end of the layer
                    * If no (double negative) we pop the next node.
                    * If yes, we break from the loop.
                 */
                if (!layerComplete)
                {
                    currentNode = _searchTree[0];
                    _searchTree.RemoveAt(0);
                }
                else
                {
                    break;
                }
            }

            /*In the situation that the LAST voxel is empty, it will fail the previous AnyUnconvertedVoxel(...) test
                * We need to allocate the currentNode into finalModelInformation.
                * In this situation, layerComplete will still be false!
                * Check should be if (AnyUnconvertedVoxels() == false) && (layerComplete == false)
            */

            if ((!(AnyUnconvertedVoxels(ref currentNode, currentHeight))) && !(layerComplete))
            {
                int rows = _modelDepth;
                int column = _modelWidth;

                //Use a temp variable to copy the information over
                int[][] layerMask = currentNode.GetArray();

                //Add the layer and its dimensions to finalModelInformation
                CopyLayer(layerMask, ref _convertedModelMask[currentHeight], rows, column);

                //Add list of blocks
                outputList[currentHeight] = new List<Vox2BloxOutputNode>(currentNode.GetListOfBricks());
            }

            //Move to next layer, reset column and rows
            currentHeight++;

            //Reset searchTree
            _searchTree.Clear();
            //Reset expanded list
            _expandedList.Clear();
        }

        //Checks if there are Unconverted voxels
        //public bool AnyUnconvertedVoxels(ref int[][] currentLayer, int layer)
        private bool AnyUnconvertedVoxels(ref Vox2BloxSearchTreeNode currentNode, int layer)
        {
            int[][] currentLayer = currentNode.GetArray();
            int currentRow = currentNode.GetCurrentRow(), currentColumn = currentNode.GetCurrentColumn();
            int rows, column;
            rows = _modelDepth;
            column = _modelWidth;
            for (int j = 0; j < rows; j++)
            {
                for (int k = 0; k < column; k++)
                {
                    if (currentLayer[j][k] == -1)
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        //Copies layer information into another array
        private void CopyLayer(int[][] layer, ref int[][] newLayer, int noOfRows, int noOfColumns)
        {
            //newLayer = new int[noOfRows][];
            for (int i = 0; i < noOfRows; i++)
            {
                newLayer[i] = new int[noOfColumns];
                for (int j = 0; j < noOfColumns; j++)
                {
                    newLayer[i][j] = layer[i][j];
                }
            }
        }

        //Builds solution from bottom left to top right
        private void BuildSolutionLeft2Right(Vox2BloxSearchTreeNode currentNode, ref int layer, ref bool layerComplete, ref List<Vox2BloxOutputNode> outputBricks)
        {
            /*This method will check if any bricks can be placed from the current position
                * First copy all relevant node information out locally. 
                  (While this can be done through the get statements, we would rather not mess with the original values)
                * Next check if the currentPosition is a unconverted convertable position.
                    * This is past on to TranversePastEmptySlots.
                    * During this stage, there is a possiblity, that we will hit the end of the layer.
                    * If this happens, layerComplete will be set to true. We need to pass all relevant information into finalModelInformation then.

                * If layerComplete is still false at this stage, we will enter the loop to check for legal bricks
                    * Encapsulated by a try/catch bracket. This is meant to catch IndexOutOfRangeExceptions. 
                      The reason is when a block (or its rotation) is too large to place in that layer. 
                      If a block attempts to be placed on an empty slot, that will fail the intended check for the block determination and not the try/catch sequence.
                    * A maskCopy is made for that layer,
                        * Change the -1 id with the currentBlockID from the currentNode as we check each position covered by the block's mask.
                        * If the checks fail, (or go out of range), maskCopy is reset so no worries.
                        * If the checks pass, and the block is valid, add the maskCopy and the accompanying information into a new Node via AddNewLayer.
                    * In addition, if the brick placed covers differnt block ids (NOT BRICKID OF BRICK CLASS!!!) for the layer below.
                        * Increment for the number of connections made to the layer below
                        
               * This is a placemarker to remind that if layerComplete is true by the above loop
                *  Pass all relevant information to finalModelInformation
                         
            */

            //Load local variables with node values
            int[][] currentLayerMask;
            int currentHeight = layer;
            int currentRow = currentNode.GetCurrentRow();
            int currentColumn = currentNode.GetCurrentColumn();
            int totalNoOfBricks = currentNode.GetNoOfBricks();
            int totalBrickValue = currentNode.GetTotalBrickWeight();
            int totalConnections = currentNode.GetTotalConnections();
            List<Vox2BloxOutputNode> currentListOfBlocks;
            if (currentNode.GetListOfBricks() != null)
            {
                currentListOfBlocks = currentNode.GetListOfBricks();
            }
            else
            {
                currentListOfBlocks = new List<Vox2BloxOutputNode>();
            }

            //Copy array over into local variable
            currentLayerMask = new int[_modelDepth][];
            for (int i = 0; i < _modelDepth; i++)
            {
                currentLayerMask[i] = new int[_modelWidth];
            }

            CopyLayer(currentNode.GetArray(), ref currentLayerMask, _modelDepth, _modelWidth);


            //Traverse past empty slots
            //Move until we reach a filled slot or end of the layer
            TraversePastEmptySlotsLeft2Right(currentLayerMask, ref layer, ref currentRow, ref currentColumn, ref layerComplete);

            //If the layer is not complete (double negative!), enter the loop
            if (!(layerComplete))
            {
                //For each legal brick
                foreach (Vox2BloxBlock brick in _legalBlocks)
                {
                    //This layer is the layer we will be working with and (attempting) to change with the block information
                    //We use a seperate variable jagged array so this information will not affect the original currentLayer variable 
                    //for this function
                    int[][] workingLayerMask;
                    //Copy array over into local variable
                    workingLayerMask = new int[_modelDepth][];

                    for (int i = 0; i < _modelDepth; i++)
                    {
                        workingLayerMask[i] = new int[_modelWidth];
                    }

                    CopyLayer(currentLayerMask, ref workingLayerMask, _modelDepth, _modelWidth);


                    //Get brick dimensions
                    int currentBrickLength = brick.GetBlockXLength();
                    int currentBrickWidth = brick.GetBlockZLength();
                    int currentBrickHeight = brick.GetBlockYLength();
                    int currentBrickWeight = brick.GetBlockWeight();
                    //Will initialise this 3d array
                    int[][][] currentBrickMask = Initialise3DArray(currentBrickHeight, currentBrickWidth, currentBrickLength);
                    //Copies relevent information over
                    currentBrickMask = brick.GetBlockMask();

                    //Flag to indicate if a brick is valid
                    //Is true until a situation proves otherwise
                    bool canPlaceBrick = true;

                    //This is a local function that notes the number of connections made by this brick
                    int connectionsMade = 0;
                    //This list takes note of the bricks that have been connected so far
                    List<int> connectedBricks = new List<int>();

                    /* We use the try catch structure to test if the block can fit in the available space.
                     * If there are any issues (main one being that the index goes out of range), it will throw to the catch statements below
                     * If these statements hit an error, it will skip and move on to the next brick
                     */
                    try
                    {
                        /* Explanation
                         * 
                         */
                        //Index used to tranverse
                        int yIndex = 0, zIndex = 0, xIndex = 0;

                        //We tranverse in layer > row > column order!!!
                        //Flag must be true for traversal to happen as well!
                        //Need to add checks for layering!
                        //Foreword: while we have a yIndex, it is not used at all. Current design does not accomodate such analysis and it may get too unwieldy!
                        while ((yIndex < currentBrickHeight) && (canPlaceBrick))
                        {
                            while ((zIndex < currentBrickWidth) && (canPlaceBrick))
                            {
                                while ((xIndex < currentBrickLength) && (canPlaceBrick))
                                {
                                    //Situations:
                                    //No issue
                                    //slot is free block occupies slot: slot == -1, block == 1
                                    //slot is free block no occupy: slot == -1, block == 0
                                    //slot empty, block empty: slot: == 0, block == 0
                                    //Slot is occupied but block does not occupy: slot != -1, block == 0

                                    //Issue!!!
                                    //slot is empty, block occupies: slot == 0, block == 1
                                    //Slot is occupied: slot != -1, block == 1

                                    /*Slot is free block occupies slot: slot == -1, block == 1
                                      Logically, we must change the position to the current id number for that node.
                                      (Not the brickID itself!)
                                     */
                                    if ((workingLayerMask[currentRow + zIndex][currentColumn + xIndex] == -1) && (currentBrickMask[yIndex][zIndex][xIndex] == 1))
                                    {
                                        //Change value of the current position to the id of the block (not the brickID from Block class!!!)
                                        workingLayerMask[currentRow + zIndex][currentColumn + xIndex] = totalNoOfBricks + 1;

                                        /*Check if any new connections are made
                                         * If the list does not contain the current ID, add it to the list
                                         * increment connections made
                                         */
                                        if (currentHeight > 0)
                                        {
                                            if (connectedBricks.Contains(_convertedModelMask[currentHeight - 1][currentRow + zIndex][currentColumn + xIndex]) == false)
                                            {
                                                connectedBricks.Add(_convertedModelMask[currentHeight - 1][currentRow + zIndex][currentColumn + xIndex]);
                                                connectionsMade++;
                                            }
                                        }

                                        xIndex++;
                                    }
                                    /*slot is empty, block occupies: slot == 0, block == 1
                                     * In this situation, the slot is empty, but the block must cover this position.
                                     * The block is definitely not valid in this case.
                                    */
                                    else if ((workingLayerMask[currentRow + zIndex][currentColumn + xIndex] == 0) && (currentBrickMask[yIndex][zIndex][xIndex] == 1))
                                    {
                                        canPlaceBrick = false;
                                    }
                                    /*slot is free block no occupy: slot == -1, block == 0
                                     * At this point, we have a situation where the slot is empty but the brick doesn't cover it.
                                     * Nothing happens
                                    */
                                    else if ((workingLayerMask[currentRow + zIndex][currentColumn + xIndex] == -1) && (currentBrickMask[yIndex][zIndex][xIndex] == 0))
                                    {
                                        xIndex++;
                                    }
                                    /*slot empty, block empty: slot: == 0, block == 0
                                     * Both slots are empty nothing needs to be done.
                                    */
                                    else if ((workingLayerMask[currentRow + zIndex][currentColumn + xIndex] == 0) && (currentBrickMask[yIndex][zIndex][xIndex] == 0))
                                    {
                                        xIndex++;
                                    }
                                    /*slot is occupied, but block empty: slot != -1, block == 0
                                     * No conflict, nothing needs to be done
                                     */
                                    else if ((workingLayerMask[currentRow + zIndex][currentColumn + xIndex] != -1) && (currentBrickMask[yIndex][zIndex][xIndex] == 0))
                                    {
                                        xIndex++;
                                    }
                                    /*Slot is occupied: slot != -1, block == 1
                                     * Block is not valid
                                     */
                                    else if ((workingLayerMask[currentRow + zIndex][currentColumn + xIndex] != -1) && (currentBrickMask[yIndex][zIndex][xIndex] == 1))
                                    {
                                        canPlaceBrick = false;
                                    }

                                }

                                //Move up a row (z-axis)
                                zIndex++;
                                xIndex = 0;
                            }

                            //Move up a layer (y-axis)
                            yIndex++;
                            zIndex = 0;
                            xIndex = 0;
                        }
                    }
                    catch (IndexOutOfRangeException ex)
                    {
                        //Console.WriteLine("Outofrange " + brick.GetBlockID());
                        //Index out of range
                        canPlaceBrick = false;
                    }
                    catch (Exception e)
                    {
                        //Other exceptions
                        canPlaceBrick = false;
                    }

                    //If the brick is valid, we can add it to the tree
                    if (canPlaceBrick)
                    {
                        /*Find valid parameters depending on the next step
                         * 1. Can move to the next column (currentColumn < totalColumn -2)
                         * 2. Else we are at the last column. Move up 1 row if possible (i.e. (currentRow < totalRow - 2)), currentColumn == 0.
                         * 3. Else, we are at the top righthand position. layerComplete is true, add layer to finalBlockInformation
                        */

                        //Create new List output node to add to new TreeNode
                        List<Vox2BloxOutputNode> newOutputList = new List<Vox2BloxOutputNode>(currentListOfBlocks);
                        Vox2BloxOutputNode newNode = new Vox2BloxOutputNode(brick.GetBlockID(), brick.GetRotation(), currentColumn, currentRow);
                        newOutputList.Add(newNode);

                        if (currentColumn <= (_modelWidth - 2))
                        {
                            AddNewLayer(ref workingLayerMask, currentRow, currentColumn + 1, totalNoOfBricks + 1, totalNoOfBricks + 1, totalBrickValue, currentBrickWeight, totalConnections + connectionsMade, newOutputList);
                        }
                        else if (currentColumn == (_modelWidth - 1))
                        {
                            if (currentRow <= (_modelDepth - 2))
                            {
                                AddNewLayer(ref workingLayerMask, currentRow + 1, 0, totalNoOfBricks + 1, totalNoOfBricks + 1, totalBrickValue, currentBrickWeight, totalConnections + connectionsMade, newOutputList);
                            }
                            else
                            {
                                //Layer is complete
                                layerComplete = true;

                                //Assign all relevant information into finalModelInformation
                                int rows = _modelDepth;
                                int column = _modelWidth;

                                //Add the layer and its dimensions to finalModelInformation
                                CopyLayer(workingLayerMask, ref _convertedModelMask[currentHeight], rows, column);

                                //Add list of blocks
                                outputBricks = new List<Vox2BloxOutputNode>(newOutputList);
                            }

                        }

                    }

                }

            }
            else
            {
                //Layer is complete
                //Assign all relevant information into finalModelInformation
                int rows = _modelDepth;
                int column = _modelWidth;

                //Use a temp variable to copy the information over
                int[][] layerMask = currentNode.GetArray();

                //Add the layer and its dimensions to finalModelInformation
                CopyLayer(layerMask, ref _convertedModelMask[currentHeight], rows, column);

                //Add list of blocks
                outputBricks = new List<Vox2BloxOutputNode>(currentListOfBlocks);
            }
        }

        //Builds solution from top right to bottom left
        private void BuildSolutionRight2Left(Vox2BloxSearchTreeNode currentNode, ref int layer, ref bool layerComplete, ref List<Vox2BloxOutputNode> outputBricks)
        {
            /*This method will check if any bricks can be placed from the current position
                * First copy all relevant node information out locally. 
                  (While this can be done through the get statements, we would rather not mess with the original values)
                * Next check if the currentPosition is a unconverted convertable position.
                    * This is past on to TranversePastEmptySlots.
                    * During this stage, there is a possiblity, that we will hit the end of the layer.
                    * If this happens, layerComplete will be set to true. We need to pass all relevant information into finalModelInformation then.

                * If layerComplete is still false at this stage, we will enter the loop to check for legal bricks
                    * Encapsulated by a try/catch bracket. This is meant to catch IndexOutOfRangeExceptions. 
                      The reason is when a block (or its rotation) is too large to place in that layer. 
                      If a block attempts to be placed on an empty slot, that will fail the intended check for the block determination and not the try/catch sequence.
                    * A maskCopy is made for that layer,
                        * Change the -1 id with the currentBlockID from the currentNode as we check each position covered by the block's mask.
                        * If the checks fail, (or go out of range), maskCopy is reset so no worries.
                        * If the checks pass, and the block is valid, add the maskCopy and the accompanying information into a new Node via AddNewLayer.
                    * In addition, if the brick placed covers differnt block ids (NOT BRICKID OF BRICK CLASS!!!) for the layer below.
                        * Increment for the number of connections made to the layer below
                        
               * This is a placemarker to remind that if layerComplete is true by the above loop
                *  Pass all relevant information to finalModelInformation
                         
            */

            //Load local variables with node values
            int[][] currentLayerMask;
            int currentHeight = layer;
            int currentRow = currentNode.GetCurrentRow();
            int currentColumn = currentNode.GetCurrentColumn();
            int totalNoOfBricks = currentNode.GetNoOfBricks();
            int totalBrickValue = currentNode.GetTotalBrickWeight();
            int totalConnections = currentNode.GetTotalConnections();

            List<Vox2BloxOutputNode> currentListOfBlocks;
            if (currentNode.GetListOfBricks() != null)
            {
                currentListOfBlocks = currentNode.GetListOfBricks();
            }
            else
            {
                currentListOfBlocks = new List<Vox2BloxOutputNode>();
            }

            //Copy array over into local variable
            currentLayerMask = new int[_modelDepth][];
            for (int i = 0; i < _modelDepth; i++)
            {
                currentLayerMask[i] = new int[_modelWidth];
            }

            CopyLayer(currentNode.GetArray(), ref currentLayerMask, _modelDepth, _modelWidth);

            //Traverse past empty slots
            //Move until we reach a filled slot or end of the layer
            TraversePastEmptySlotsRight2Left(currentLayerMask, ref layer, ref currentRow, ref currentColumn, ref layerComplete);

            //If the layer is not complete (double negative!), enter the loop
            if (!(layerComplete))
            {
                //For each legal brick
                foreach (Vox2BloxBlock brick in _legalBlocks)
                {
                    //This layer is the layer we will be working with and (attempting) to change with the block information
                    //We use a seperate variable jagged array so this information will not affect the original currentLayer variable 
                    //for this function
                    int[][] workingLayerMask;
                    //Copy array over into local variable
                    workingLayerMask = new int[_modelDepth][];
                    for (int i = 0; i < _modelDepth; i++)
                    {
                        workingLayerMask[i] = new int[_modelWidth];
                    }

                    CopyLayer(currentLayerMask, ref workingLayerMask, _modelDepth, _modelWidth);


                    //Get brick dimensions
                    int currentBrickLength = brick.GetBlockXLength();
                    int currentBrickWidth = brick.GetBlockZLength();
                    int currentBrickHeight = brick.GetBlockYLength();
                    int currentBrickWeight = brick.GetBlockWeight();
                    //Will initialise this 3d array
                    int[][][] currentBrickMask = Initialise3DArray(currentBrickHeight, currentBrickWidth, currentBrickLength);
                    //Copies relevent information over
                    currentBrickMask = brick.GetBlockMask();

                    //Flag to indicate if a brick is valid
                    //Is true until a situation proves otherwise
                    bool canPlaceBrick = true;

                    //This is a local function that notes the number of connections made by this brick
                    int connectionsMade = 0;
                    //This list takes note of the bricks that have been connected so far
                    List<int> connectedBricks = new List<int>();

                    /* We use the try catch structure to test if the block can fit in the available space.
                     * If there are any issues (main one being that the index goes out of range), it will throw to the catch statements below
                     * If these statements hit an error, it will skip and move on to the next brick
                     */
                    try
                    {
                        /* Explanation
                         * 
                         */
                        //Index used to tranverse
                        int yIndex = 0, zIndex = 0, xIndex = 0;

                        //We tranverse in layer > row > column order!!!
                        //Flag must be true for traversal to happen as well!
                        //Need to add checks for layering!
                        //Foreword: while we have a yIndex, it is not used at all. Current design does not accomodate such analysis and it may get too unwieldy!
                        while ((yIndex < currentBrickHeight) && (canPlaceBrick))
                        {
                            while ((zIndex < currentBrickWidth) && (canPlaceBrick))
                            {
                                while ((xIndex < currentBrickLength) && (canPlaceBrick))
                                {
                                    //Situations:
                                    //No issue
                                    //slot is free block occupies slot: slot == -1, block == 1
                                    //slot is free block no occupy: slot == -1, block == 0
                                    //slot empty, block empty: slot: == 0, block == 0
                                    //Slot is occupied but block does not occupy: slot != -1, block == 0

                                    //Issue!!!
                                    //slot is empty, block occupies: slot == 0, block == 1
                                    //Slot is occupied: slot != -1, block == 1

                                    /*Slot is free block occupies slot: slot == -1, block == 1
                                      Logically, we must change the position to the current id number for that node.
                                      (Not the brickID itself!)
                                     */
                                    if ((workingLayerMask[currentRow - zIndex][currentColumn - xIndex] == -1) && (currentBrickMask[yIndex][zIndex][xIndex] == 1))
                                    {
                                        //Change value of the current position to the id of the block (not the brickID from Block class!!!)
                                        workingLayerMask[currentRow - zIndex][currentColumn - xIndex] = totalNoOfBricks + 1;

                                        /*Check if any new connections are made
                                         * If the list does not contain the current ID, add it to the list
                                         * increment connections made
                                         */
                                        if (currentHeight > 0)
                                        {
                                            if (connectedBricks.Contains(_convertedModelMask[currentHeight - 1][currentRow - zIndex][currentColumn - xIndex]) == false)
                                            {
                                                connectedBricks.Add(_convertedModelMask[currentHeight - 1][currentRow - zIndex][currentColumn - xIndex]);
                                                connectionsMade++;
                                            }
                                        }

                                        xIndex++;
                                    }
                                    /*slot is empty, block occupies: slot == 0, block == 1
                                     * In this situation, the slot is empty, but the block must cover this position.
                                     * The block is definitely not valid in this case.
                                    */
                                    else if ((workingLayerMask[currentRow - zIndex][currentColumn - xIndex] == 0) && (currentBrickMask[yIndex][zIndex][xIndex] == 1))
                                    {
                                        canPlaceBrick = false;
                                    }
                                    /*slot is free block no occupy: slot == -1, block == 0
                                     * At this point, we have a situation where the slot is empty but the brick doesn't cover it.
                                     * Nothing happens
                                    */
                                    else if ((workingLayerMask[currentRow - zIndex][currentColumn - xIndex] == -1) && (currentBrickMask[yIndex][zIndex][xIndex] == 0))
                                    {
                                        xIndex++;
                                    }
                                    /*slot empty, block empty: slot: == 0, block == 0
                                     * Both slots are empty nothing needs to be done.
                                    */
                                    else if ((workingLayerMask[currentRow - zIndex][currentColumn - xIndex] == 0) && (currentBrickMask[yIndex][zIndex][xIndex] == 0))
                                    {
                                        xIndex++;
                                    }
                                    /*slot is occupied, but block empty: slot != -1, block == 0
                                     * No conflict, nothing needs to be done
                                     */
                                    else if ((workingLayerMask[currentRow - zIndex][currentColumn - xIndex] != -1) && (currentBrickMask[yIndex][zIndex][xIndex] == 0))
                                    {
                                        xIndex++;
                                    }
                                    /*Slot is occupied: slot != -1, block == 1
                                     * Block is not valid
                                     */
                                    else if ((workingLayerMask[currentRow - zIndex][currentColumn - xIndex] != -1) && (currentBrickMask[yIndex][zIndex][xIndex] == 1))
                                    {
                                        canPlaceBrick = false;
                                    }

                                }

                                //Move up a row (z-axis)
                                zIndex++;
                                xIndex = 0;
                            }

                            //Move up a layer (y-axis)
                            yIndex++;
                            zIndex = 0;
                            xIndex = 0;
                        }
                    }
                    catch (IndexOutOfRangeException ex)
                    {
                        //Console.WriteLine("Outofrange " + brick.GetBlockID());
                        //Index out of range
                        canPlaceBrick = false;
                    }
                    catch (Exception e)
                    {
                        //Other exceptions
                        canPlaceBrick = false;
                    }

                    //If the brick is valid, we can add it to the tree
                    if (canPlaceBrick)
                    {
                        /*Find valid parameters depending on the next step
                         * 1. Can move to the next column (currentColumn < totalColumn -2)
                         * 2. Else we are at the last column. Move up 1 row if possible (i.e. (currentRow < totalRow - 2)), currentColumn == 0.
                         * 3. Else, we are at the top righthand position. layerComplete is true, add layer to finalBlockInformation
                        */

                        //Create new List output node to add to new TreeNode
                        List<Vox2BloxOutputNode> newOutputList = new List<Vox2BloxOutputNode>(currentListOfBlocks);
                        Vox2BloxOutputNode newNode = new Vox2BloxOutputNode(brick.GetBlockID(), brick.GetRotation(), currentColumn, currentRow);
                        newOutputList.Add(newNode);

                        if (currentColumn > 0)
                        {
                            AddNewLayer(ref workingLayerMask, currentRow, currentColumn - 1, totalNoOfBricks + 1, totalNoOfBricks + 1, totalBrickValue, currentBrickWeight, totalConnections + connectionsMade, newOutputList);
                        }
                        else //if (currentColumn == 0)
                        {
                            if (currentRow > 0)
                            {
                                AddNewLayer(ref workingLayerMask, currentRow - 1, _modelWidth - 1, totalNoOfBricks + 1, totalNoOfBricks + 1, totalBrickValue, currentBrickWeight, totalConnections + connectionsMade, newOutputList);
                            }
                            else //if ((currentColumn == 0) && (currentRow == 0))
                            {
                                //Layer is complete
                                layerComplete = true;

                                //Assign all relevant information into finalModelInformation
                                int rows = _modelDepth;
                                int column = _modelWidth;

                                //Add the layer and its dimensions to finalModelInformation
                                CopyLayer(workingLayerMask, ref _convertedModelMask[currentHeight], rows, column);

                                //Add current list of blocks
                                outputBricks = new List<Vox2BloxOutputNode>(newOutputList);
                            }
                        }
                    }
                }
            }
            else
            {
                //Layer is complete
                //Assign all relevant information into finalModelInformation
                int rows = _modelDepth;
                int column = _modelWidth;

                //Use a temp variable to copy the information over
                int[][] layerMask = currentNode.GetArray();

                //Add the layer and its dimensions to finalModelInformation
                CopyLayer(layerMask, ref _convertedModelMask[currentHeight], rows, column);

                //Add current list of blocks
                //if (currentListOfBlocks == null) { Console.WriteLine("exists"); }
                outputBricks = new List<Vox2BloxOutputNode>(currentListOfBlocks);
            }
        }

        /* This method is called when the pointer to the current position within the 3D Jagged Array is an empty position
         Will keep traversing until a valid non-converted position is found,
         If it hits the end of the row, reset column pointer back to the start of the row, and move up one row
         If we are at the top most row and at the last column (i.e. the top most right position) we have reached the end of the layer.
            * Send the layer information as final.
            * Reset all pointer information!
        */
        private void TraversePastEmptySlotsLeft2Right(int[][] currentLayer, ref int currentHeight, ref int currentRow, ref int currentColumn, ref bool layerComplete)
        {
            while (currentLayer[currentRow][currentColumn] != -1)
            {
                //Check position
                if (currentColumn < (_modelWidth - 1))
                {
                    //If not at last position of the row
                    currentColumn++;
                }
                //Is at last position of the row
                else if (currentRow < (_modelDepth - 1))
                {
                    //Move up a row, reset column pointer
                    currentRow++;
                    currentColumn = 0;
                }
                //Is at the last position
                else //if ((currentRow == 0) && (currentColumn == 0))
                {
                    /*Set all information accordingly
                        * layerComplete == true
                        * CurrentRow and CurrentColumn should be reset. (However, at the end of the row, this may not be necessary!)
                        * No need to increment height. This done automatically if layerComplete is true 
                          or if AnyUnconvertedVoxels == false (Something which should now be triggered)
                    */
                    layerComplete = true;
                }
            }
        }

        private void TraversePastEmptySlotsRight2Left(int[][] currentLayer, ref int currentHeight, ref int currentRow, ref int currentColumn, ref bool layerComplete)
        {
            while (currentLayer[currentRow][currentColumn] != -1)
            {
                //if (currentRow == 0 && currentColumn == 1) { Console.WriteLine("yeah boi"); }
                //Check position
                if (currentColumn > 0)
                {
                    //If not at last position of the row
                    currentColumn--;
                }
                //Is at first position of the row
                else
                {
                    if (currentRow > 0)
                    {
                        //Move up a row, reset column pointer
                        currentRow--;
                        currentColumn = _modelWidth - 1;
                    }
                    //Is at the last position
                    else
                    {
                        /*Set all information accordingly
                            * layerComplete == true
                            * CurrentRow and CurrentColumn should be reset. (However, at the end of the row, this may not be necessary!)
                            * No need to increment height. This done automatically if layerComplete is true 
                              or if AnyUnconvertedVoxels == false (Something which should now be triggered)
                        */
                        //Console.WriteLine("WTF");
                        layerComplete = true;
                    }
                }
            }
        }

        //Initialises a 3D JaggedArray
        public int[][][] Initialise3DArray(int height, int row, int column)
        {
            int[][][] tempArray;
            tempArray = new int[height][][];

            for (int y = 0; y < height; y++)
            {
                tempArray[y] = new int[row][];
                for (int z = 0; z < row; z++)
                {
                    tempArray[y][z] = new int[column];
                    for (int x = 0; x < column; x++)
                    {
                        tempArray[y][z][x] = 0;
                    }
                }
            }

            //Return the array back to the variable that called it.
            return tempArray;
        }

        //Debugging Function
        private void GetBlocksNo()
        {
            Console.WriteLine("no of blocks: " + _legalBlocks.Count);
        }


        // MESSAGE PASSING INTERFACE IMPLEMENTATION

        private List<IObserver> _observers = new List<IObserver>();
        private Message _message;

        public void RegisterObserver(IObserver o)
        {
            _observers.Add(o);
        }

        public void RemoveObserver(IObserver o)
        {
            _observers.Remove(o);
        }

        public void NotifyObservers()
        {
            foreach (var obs in _observers)
            {
                obs.UpdateMessage(_message);
            }
        }

        private void CreateMessage(string title, string content)
        {
            if (_observers.Count != 0)
            {
                _message = new Message(title, content);
                NotifyObservers();
            }
        }

        // DEBUG

        public string GetModelData()
        {
            return _voxelisedModel.GetMapData();
        }

        public string ListToString()
        {
            List<Vox2BloxOutputNode>[] outputList = ConvertVoxelToBricks();

            int i = 0;

            string returnData = null;

            foreach (List<Vox2BloxOutputNode> layer in outputList)
            {
                returnData += "Layer " + i.ToString();

                foreach(Vox2BloxOutputNode node in layer)
                {
                    if (node != null)
                    {
                        returnData += node.GetBlockID().ToString() + " " + node.GetXPosition().ToString() + " " + node.GetYPosition().ToString() + " " + node.GetRotation().ToString() + "\n";
                    }
                    else
                    {
                        returnData += "NULL \n";
                    }

                }

                i++;

                returnData += "\n";

            }

            return returnData;

        }


        public string PrintConvertedMap()
        {
            int rows, column;
            rows = _modelDepth;
            column = _modelWidth;

            string returnData = null;

            for (int h = 0; h < _modelHeight; h++)
            {
                Console.WriteLine("Layer " + h + ":");

                returnData += "Layer " + h.ToString() + ": \n";

                for (int i = 0; i < rows; i++)
                {
                    Console.Write(i + " | ");

                    returnData += i.ToString() + " | ";


                    for (int j = 0; j < column; j++)
                    {
                        if (_convertedModelMask[h][i][j] != -1)
                        {
                            Console.Write("{0, 3} ", _convertedModelMask[h][i][j]);

                            returnData += _convertedModelMask[h][i][j].ToString();
        }
                        else
                        {
                            Console.Write("{0, 3} ", "+");

                            returnData += "*";
                        }
                    }
                    Console.Write(Environment.NewLine);
                    returnData += "\n";

                }
                Console.Write(Environment.NewLine);
                returnData += "\n";
            }

            return returnData;
        }

    }





}
