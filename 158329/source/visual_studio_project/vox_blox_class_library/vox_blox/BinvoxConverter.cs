﻿using System;
using System.Collections.Generic;
using System.Text;

namespace vox_blox
{
    public class BinvoxConverter
    {
       
        public static void ConvertObjToBinvox(string binvoxDirectory, string filePathNoExt, int dim, bool hollow)
        {
            // Converts a .obj file to .binvox using external binvox executable, via a command line statement

            string binvoxDirEsc = binvoxDirectory.Replace(@"\", @"\\");
            string filePathEsc = filePathNoExt.Replace(@"\", @"\\");

            // Set exact voxelization if required (any voxel with part of a triangle gets set)
            // This setting will also hollow the model
            string eSet = (hollow) ? "-e" : "";

            Cmd cmd = new Cmd();
            cmd.CreateCmdProcess(new string[] 
                    {
                        binvoxDirEsc + "\\binvox.exe",      // Executable program to run
                        eSet,                               // Exact voxelization
                        "-d " + dim.ToString(),             // Requested dimensions of the voxelized model (cubed)
                        "-rotx",                            // Apply a 90 degree rotation in X axis to compensate for default .obj orientation
                        filePathEsc + ".obj"                // .obj file to convert
                    });                       
            cmd.RunProcess();
        }

        public static void ConvertBinvoxToTxt(string binvoxDirectory, string filePathNoExt)
        {
            // Converts a .binvox file to .txt using external binvox_to_txt executable, via a command line statement

            string binvoxDirEsc = binvoxDirectory.Replace(@"\", @"\\");
            string filePathEsc = filePathNoExt.Replace(@"\", @"\\");

            Cmd cmd = new Cmd();
            cmd.CreateCmdProcess(new string[] 
                    {
                        binvoxDirEsc + "\\binvox_to_txt.exe",       // Executable program to run
                        filePathEsc + ".binvox"                     // .binvox file to convert
                    });
            cmd.RunProcess();
        }

        public static void ConvertObjToTxt(string binvoxDirectory, string filePathNoExt, int dim, bool hollow)
        {
            // Chains commands ConvertObjToBinvox && ConvertBinvoxToTxt
            // Ensures the second command isn't processed until the first completes

            string binvoxDirEsc = binvoxDirectory.Replace(@"\", @"\\");
            string filePathEsc  = filePathNoExt.Replace(@"\", @"\\");

            // Set exact voxelization if required - any voxel with part of a triangle gets set (optional)
            // This setting will also hollow the model
            string eSet = (hollow) ? "-e" : "";

            Cmd cmd = new Cmd();
            cmd.CreateCmdProcess(new string[] 
                    {
                        // Execute the .obj to .binvox conversion
                        binvoxDirEsc + "\\binvox.exe",
                        "-d " + dim.ToString(),
                        eSet,
                        "-rotx",
                        filePathEsc + ".obj",
                        // Wait until complete
                        "&&",
                        // Execute the .binvox to .txt conversion
                        binvoxDirEsc + "\\binvox_to_txt.exe",
                        filePathEsc + ".binvox"
                    });
            cmd.RunProcess();
        }

    }

}
