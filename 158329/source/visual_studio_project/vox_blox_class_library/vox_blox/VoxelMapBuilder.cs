﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Text;

namespace vox_blox
{
    public static class VoxelMapBuilder
    {
        // Builds a Grid3D object from a voxel .txt file, which is essentially a voxel map of the model being imported

        public static Grid3D ConvertTxtToArray(string filepath)
        {
            if (File.Exists(filepath)) {

                int height = 0;
                int width  = 0;
                int depth  = 0;

                List<int[]> tempVoxelData = new List<int[]>();

                try
                {

                    string[] text = File.ReadAllLines(filepath);

                    // Read each line and parse as required
                    foreach (string line in text)
                    {

                        if (line.Contains("dim"))   // Check for dimensions first
                        {
                            // This line contains the dimension data of the voxel grid
                            // "depth, width, and height of the voxel grid should all be equal" - http://www.patrickmin.com/binvox/binvox.html
                            int[] dimensions = GetDimensions(line);
                            if (dimensions != null)
                            {
                                // Set voxel grid values
                                height = dimensions[0];
                                width  = dimensions[1];
                                depth  = dimensions[2];

                            }
                            else
                            {
                                // An error occurred
                                return null;
                            }

                        }
                        else if (!line.Contains("#"))
                        {
                            // This is a line of voxel data - parse and add into list
                            // It is not a comment (does not contain '#') and cannot be voxel grid dimensions as this was previously checked
                            int[] voxels = ConvertToIntArray(line);
                            if (voxels != null)
                            {
                                tempVoxelData.Add(voxels);
                            }
                            else
                            {
                                // There was an error
                                return null;
                            }
                        }

                    }

                    // The .txt file created by binvox_to_txt.exe has the voxel data in the order: x, z, y (fastest to slowest running)
                    // Our algorithms traverse the grid in the order: z, x, y (fastest to slowest running)
                    // This needs to be taken into account when adding elements to the Grid3D object

                    Console.WriteLine("Building Grid3D");

                    Grid3D voxelMap = new Grid3D(height, width, depth);     // Object which will be returned by this algorithm

                    int x = 0;  // Width index
                    int y = 0;  // Height index

                    foreach(int[] voxelLine in tempVoxelData)
                    {

                        // Each element in the array corresponds to a voxel value in the depth of the model
                        for(int z = 0; z < depth; z++)
                        {
                            int value = voxelLine[z];               // Get the value at the depth index (z)
                            voxelMap.SetElement(y, z, x, value);    // Add this to the Grid3D object, swapping x and z for correct orientation in the output
                        }

                        // Update the indices
                        y++;                    // Increment the height
                        if(y >= height)         // Check the height has not reached the maximum
                        {                       // If is has...
                            y = 0;              // Reset the height to 0
                            x++;                // Increment the width

                            if(x >= width)      // Check the width has not reached the maximum
                            {                   // If is has...
                                x = 0;          // Reset the width to 0
                            }

                        }

                    }

                    // Once the loop is finished, the voxel map has been built and the process is complete
                    return voxelMap;

                }
                catch (Exception)
                {
                    // An error during conversion
                    return null;
                }

            }
            else
            {
                // File does not exist
                return null;
            }

        }

        private static int[] GetDimensions(string line)
        {
            // Given a string containing voxel grid dimension data, returns an array with the integer dimension values
            try
            {
                int[] returnData = new int[3];

                string[] tempData = line.Split(null);

                returnData[0] = Convert.ToInt32(tempData[1]);
                returnData[1] = Convert.ToInt32(tempData[2]);
                returnData[2] = Convert.ToInt32(tempData[3]);

                return returnData;
            }
            catch (Exception e)
            {
                // An error occurred
                Console.WriteLine(e.Message);
                return null;
            }
        }

        private static int[] ConvertToIntArray(string line)
        {
            // Converts a string containing voxel values into an int array

            string[] splitLine = line.Split((char[])null, StringSplitOptions.RemoveEmptyEntries);
            int size = splitLine.Length;
            int[] returnData = new int[size];

            if (size == 0) return null;

            for(int i = 0; i < size; i++) 
            {
                try
                {
                    returnData[i] = Convert.ToInt32(splitLine[i]);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    return null;
                }

            }

            return returnData;

        }

    }

}
