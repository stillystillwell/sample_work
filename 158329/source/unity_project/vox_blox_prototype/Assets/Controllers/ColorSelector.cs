﻿using UnityEngine;
using System.Collections;

public static class ColorSelector {

    public enum CustomColors
    {
        Black,      // 0
        Grey,       // 1
        White,      // 2
        Red,        // 3
        Orange,     // 4
        Yellow,     // 5
        Green,      // 6
        Blue,       // 7
        Purple,     // 8
        Pink        // 9
    }

    public static Color GetColor(CustomColors color)
    {
        switch (color)
        {
            case CustomColors.Black:
                return new Color(0.050f, 0.050f, 0.050f, 1.0f);

            case CustomColors.Grey:
                return new Color(0.545f, 0.545f, 0.545f, 1.0f);

            case CustomColors.White:
                return new Color(1.000f, 1.000f, 1.000f, 1.0f);

            case CustomColors.Red:
                return new Color(0.784f, 0.000f, 0.000f, 1.0f);

            case CustomColors.Orange:
                return new Color(1.000f, 0.372f, 0.094f, 1.0f);

            case CustomColors.Yellow:
                return new Color(1.000f, 0.831f, 0.000f, 1.0f);

            case CustomColors.Green:
                return new Color(0.200f, 0.858f, 0.000f, 1.0f);

            case CustomColors.Blue:
                return new Color(0.000f, 0.337f, 0.658f, 1.0f);

            case CustomColors.Purple:
                return new Color(0.623f, 0.000f, 0.831f, 1.0f);

            case CustomColors.Pink:
                return new Color(0.905f, 0.200f, 0.682f, 1.0f);

            default:    // White
                return new Color(1.000f, 1.000f, 1.000f, 1.0f);

        }

    }
    public static Color GetColor(string s)
    {
        if (s == "Black")
        {
            return GetColor(ColorSelector.CustomColors.Black);
        }
        else if (s == "Grey")
        {
            return GetColor(ColorSelector.CustomColors.Grey);
        }
        else if (s == "White")
        {
            return GetColor(ColorSelector.CustomColors.White);
        }
        else if (s == "Red")
        {
            return GetColor(ColorSelector.CustomColors.Red);
        }
        else if (s == "Orange")
        {
            return GetColor(ColorSelector.CustomColors.Orange);
        }
        else if (s == "Yellow")
        {
            return GetColor(ColorSelector.CustomColors.Yellow);
        }
        else if (s == "Green")
        {
            return GetColor(ColorSelector.CustomColors.Green);
        }
        else if (s == "Blue")
        {
            return GetColor(ColorSelector.CustomColors.Blue);
        }
        else if (s == "Purple")
        {
            return GetColor(ColorSelector.CustomColors.Purple);
        }
        else if (s == "Pink")
        {
            return GetColor(ColorSelector.CustomColors.Pink);
        }
        else
        {
            // Invalid selection - return default value
            return GetColor(ColorSelector.CustomColors.White);
        }

    }


    public static int GetColorID(string s)
    {
        if (s == "Black")
        {
            return (int)ColorSelector.CustomColors.Black;
        }
        else if (s == "Grey")
        {
            return (int)ColorSelector.CustomColors.Grey;
        }
        else if (s == "White")
        {
            return (int)ColorSelector.CustomColors.White;
        }
        else if (s == "Red")
        {
            return (int)ColorSelector.CustomColors.Red;
        }
        else if (s == "Orange")
        {
            return (int)ColorSelector.CustomColors.Orange;
        }
        else if (s == "Yellow")
        {
            return (int)ColorSelector.CustomColors.Yellow;
        }
        else if (s == "Green")
        {
            return (int)ColorSelector.CustomColors.Green;
        }
        else if (s == "Blue")
        {
            return (int)ColorSelector.CustomColors.Blue;
        }
        else if (s == "Purple")
        {
            return (int)ColorSelector.CustomColors.Purple;
        }
        else if (s == "Pink")
        {
            return (int)ColorSelector.CustomColors.Pink;
        }
        else
        {
            // Invalid selection - return default value
            return -1;
        }

    }

}
