﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using vox_blox;
using System;

public class UI_Manager : MonoBehaviour {

    // UI COMPONENTS

    // Cameras
    public GameObject _model_import_camera;
    // Canvases
    public GameObject _block_selector_canvas;
    public GameObject _build_menu_canvas;
    public GameObject _colour_selector_canvas;
    public GameObject _game_mode_canvas;
    // public GameObject _delete_menu_canvas;
    public GameObject _layer_menu_canvas;
    public GameObject _layer_options_canvas;
    public GameObject _model_import_canvas;
    public GameObject _message_window_canvas;
    public GameObject _perspective_tool_canvas;
    public GameObject _project_menu_canvas;
    // Input Fields
    public InputField _layer_height_input;
    // Panels
    public GameObject _build_menu_panel;
    public GameObject _layer_menu_panel;
    public GameObject _block_selector_panel;
    // Sliders
    public Slider _layer_height_slider;
    private const int _max_layer_height = 256;
    // Text 
    public Text _layer_height_text;
    public Text _game_mode_text;
    public Text _imported_model_filename;
    public Text _message_window_title;
    public Text _message_window_text;
    private string _default_model_filename = "e.g. my3Dmodel.obj";
    // Toggles (Checkboxes)
    public Toggle _hollow_model_checkbox;

    // SINGELTON SETUP

    private static UI_Manager instance = null;

    public static UI_Manager Instance
    {
        get { return instance; }
    }

    private void SetThisManager()
    {
        // This enforces the singleton pattern, meaning there can only ever be one instance of a Manager.
        if (instance == null)

            instance = this;

        else if (instance != null)

            Destroy(this.gameObject);

        DontDestroyOnLoad(this.gameObject);
    }

    // MONOBEHAVIOUR METHODS

    void Awake()
    {
        SetThisManager();
    }

    private void Start()
    {
        SetDefaultUI();
        InitSliders();
    }

    private void Update()
    {
        CheckLayerHeight();
        CheckGameMode();
    }

    // INITIALIZATION & UPDATE

    private void InitSliders()
    {
        // Layer Height Slider [Model Import Menu]
        _layer_height_slider.minValue = 1;                      // Minimum height an imported model can be voxelized to
        _layer_height_slider.maxValue = _max_layer_height;      // Maximum height an imported model can be voxelized to
        _layer_height_slider.value = 42;                        // Default value for layer height
        _layer_height_slider.wholeNumbers = true;               // No decimal values
        _layer_height_slider.onValueChanged.AddListener(delegate { LayerHeightSliderMoved(); });    // Add a listener which is triggered when slider is moved
        _layer_height_input.text = _layer_height_slider.value.ToString();                           // Update text in associated input field
        _layer_height_input.onEndEdit.AddListener(delegate { LayerHeightInputReceived(); });        // Add a listener which is triggered when value of the input field is updated and return key is pressed
    }

    private void CheckLayerHeight()
    {
        _layer_height_text.text = Workspace_Manager.Instance.GetCurrentLayer().ToString();
    }

    private void CheckGameMode()
    {

        if (Workspace_Manager.Instance.GetComponent<Workspace_Cursor>().IsEditMode())
        {
            if (Workspace_Manager.Instance.GetComponent<Workspace_Cursor>().IsCreateMode())
            {
                _game_mode_text.text = "MODE: BUILD";
            }
            else
            {
                _game_mode_text.text = "MODE: DELETE";
            }

        }
        else
        {
            _game_mode_text.text = "MODE: MENU";
        }

    }

    public void UpdateMessageWindowText(Message m)
    {
        // Sets the text elements of the message window
        _message_window_title.text = m.getTitle();
        _message_window_text.text = m.getContent();
    }

    // PROJECT MENU FUNCTIONS

    public void NewProject()                { Workspace_Manager.Instance.SetupNewProject(); }
    public void OpenProject()               { Workspace_Manager.Instance.LoadProjectFromFile(); }
    public void SaveProject()               { Workspace_Manager.Instance.SaveProjectToFile(); }
    public void ExportProject()             { Workspace_Manager.Instance.ExportProjectData(); }

    // COLOR SELECTOR

    public void SetActiveBlockColor(int enumVal)
    {
        Color color = ColorSelector.GetColor((ColorSelector.CustomColors)enumVal);
        Workspace_Manager.Instance.SetActiveBlockColor((ColorSelector.CustomColors)enumVal);
        Import_Manager.Instance.SetModelColor((ColorSelector.CustomColors)enumVal);
    }

    // BLOCK SELECTOR

    public void ScrollBlockSelectorRight()
    {
        float stdButtonWidth = 50;
        float stdScreenWidth = 800;
        float targetScreenWidth = Screen.width;
        float moveValue = stdButtonWidth * targetScreenWidth / stdScreenWidth;
        Vector3 transformValue = new Vector3(moveValue, 0, 0);

        if (_block_selector_panel.transform.position.x < 0)
        {
            _block_selector_panel.transform.position += transformValue;
        }
        
    }

    public void ScrollBlockSelectorLeft()
    {
        float stdButtonWidth = 50;
        float stdScreenWidth = 800;
        float targetScreenWidth = Screen.width;
        float moveValue = stdButtonWidth * targetScreenWidth / stdScreenWidth;
        Vector3 transformValue = new Vector3(moveValue, 0, 0);

        if (_block_selector_panel.transform.position.x > -(moveValue*2))
        {
            _block_selector_panel.transform.position -= transformValue;
        }

    }

    // IMPORT MODEL UI

    public void ClickSelectFile()           { Import_Manager.Instance.SelectObjFile(); }
    public void ClickImportProceed()        { Import_Manager.Instance.ProcessModelImport(); }
    public void ClickImportCancel()         { Import_Manager.Instance.CancelModelImport(); }
    public void ClickIncreaseRoll()         { Import_Manager.Instance.IncreaseModelRoll(); }
    public void ClickDecreaseRoll()         { Import_Manager.Instance.DecreaseModelRoll(); }
    public void ClickIncreasePitch()        { Import_Manager.Instance.IncreaseModelPitch(); }
    public void ClickDecreasePitch()        { Import_Manager.Instance.DecreaseModelPitch(); }
    public void ClickIncreaseYaw()          { Import_Manager.Instance.IncreaseModelYaw(); }
    public void ClickDecreaseYaw()          { Import_Manager.Instance.DecreaseModelYaw(); }
    public void ClickResetRotation()        { Import_Manager.Instance.ResetModelRotation(); }
    public void SetFilenameText(string s)   { _imported_model_filename.text = s; }
    public void ResetFilenameText()         { _imported_model_filename.text = _default_model_filename; }
    public int GetLayerHeight()             { return Convert.ToInt32(_layer_height_slider.value); }
    public bool HollowModelIsChecked()      { return _hollow_model_checkbox.isOn; }

    private void LayerHeightSliderMoved()
    {
        // Updates the value of the inputfield based on slider position
        _layer_height_input.text = _layer_height_slider.value.ToString();
    }

    private void LayerHeightInputReceived()
    {
        // Updates the value of the layer height slider based on the input text received
        int requestedHeight = Convert.ToInt32(_layer_height_input.text);
        _layer_height_slider.value = (requestedHeight > _max_layer_height) ? 256 : requestedHeight;

    }

    // LAYER MENU

    public void MoveLayerUp()               { Workspace_Manager.Instance.MoveWorkspaceLayerUp();  }
    public void MoveLayerDown()             { Workspace_Manager.Instance.MoveWorkspaceLayerDown(); }
    public void ToggleDisplayLayerAbove()   { Workspace_Manager.Instance.ToggleViewLayersAbove(); }
    public void ToggleDisplayLayerBelow()   { Workspace_Manager.Instance.ToggleViewLayersBelow(); }
    public void ToggleDisplayGrid()         { Workspace_Manager.Instance.GetComponent<Workspace_Grid>().ToggleGridVisibility();  }

    // MENU VISIBILITY

    public void ActivateBlockSelector()         { _block_selector_canvas.SetActive(true); }
    public void DeactivateBlockSelector()       { _block_selector_canvas.SetActive(false); }
    public void ToggleBlockSelector()           { _block_selector_canvas.SetActive(!_block_selector_canvas.activeInHierarchy); }

    public void ActivateBuildMenu()             { _build_menu_canvas.SetActive(true); }
    public void DeactivateBuildMenu()           { _build_menu_canvas.SetActive(false); }
    public void ToggleBuildMenu()
    {
        if (_build_menu_panel.activeInHierarchy)
        {
            DeactivateBuildMenuPanel();
            DeactivateBlockSelector();
            DeactivateColourSelector();
        }
        else
        {
            ActivateBuildMenuPanel();
        }
    }

    public void ActivateBuildMenuPanel()        { _build_menu_panel.SetActive(true); }
    public void DeactivateBuildMenuPanel()      { _build_menu_panel.SetActive(false); }

    public void ActivateColourSelector()        { _colour_selector_canvas.SetActive(true); }
    public void DeactivateColourSelector()      { _colour_selector_canvas.SetActive(false); }
    public void ToggleColourSelector()          { _colour_selector_canvas.SetActive(!_colour_selector_canvas.activeInHierarchy); }

    public void ActivateGameModeCanvas()        { _game_mode_canvas.SetActive(true); }
    public void DeactivateGameModeCanvas()      { _game_mode_canvas.SetActive(false); }

    public void ActivateLayerMenu()             { _layer_menu_canvas.SetActive(true); }
    public void DeactivateLayerMenu()           { _layer_menu_canvas.SetActive(false); }
    public void ToggleLayerMenu()
    {
        if (_layer_menu_panel.activeInHierarchy)
        {
            DeactivateLayerMenuPanel();
            DeactivateLayerOptions();
        }
        else
        {
            ActivateLayerMenuPanel();
        }
    }

    public void ActivateLayerMenuPanel()        { _layer_menu_panel.SetActive(true); }
    public void DeactivateLayerMenuPanel()      { _layer_menu_panel.SetActive(false); }

    public void ActivateLayerOptions()          { _layer_options_canvas.SetActive(true); }
    public void DeactivateLayerOptions()        { _layer_options_canvas.SetActive(false); }
    public void ToggleLayerOptions()            { _layer_options_canvas.SetActive(!_layer_options_canvas.activeInHierarchy); }

    public void ActivateModelImportMenu()
    {
        _model_import_canvas.SetActive(true);
        _model_import_camera.SetActive(true);
        Import_Manager.Instance.ResetModelImport();
    }
    public void DeactivateModelImportMenu()
    {
        _model_import_canvas.SetActive(false);
        _model_import_camera.SetActive(false);
    }

    public void ActivateMessageWindow()         { _message_window_canvas.SetActive(true); }
    public void DeactivateMessageWindow()       { _message_window_canvas.SetActive(false); }
    public void ToggleMessageWindow()           { _message_window_canvas.SetActive(!_message_window_canvas.activeInHierarchy); }

    public void ActivatePerspectiveTool()       { _perspective_tool_canvas.SetActive(true); }
    public void DeactivatePerspectiveTool()     { _perspective_tool_canvas.SetActive(false); }

    public void ActivateProjectMenu()           { _project_menu_canvas.SetActive(true); }
    public void DeactivateProjectMenu()         { _project_menu_canvas.SetActive(false); }

    public void SetDefaultUI()
    {
        // Resets UI elements to default view
        HideAllUI();
        ActivateBuildMenu();
        ActivateBuildMenuPanel();
        ActivateGameModeCanvas();
        ActivateLayerMenu();
        ActivateLayerMenuPanel();
        // ActivatePerspectiveTool();
        ActivateProjectMenu();
    }

    public void HideAllUI()
    {
        // Hides all UI elements
        DeactivateBlockSelector();
        DeactivateBuildMenu();
        DeactivateBuildMenuPanel();
        DeactivateColourSelector();
        DeactivateGameModeCanvas();
        DeactivateLayerMenu();
        DeactivateLayerMenuPanel();
        DeactivateLayerOptions();
        DeactivateMessageWindow();
        DeactivateModelImportMenu();
        DeactivatePerspectiveTool();
        DeactivateProjectMenu();
    }

}
