﻿using UnityEngine;
using System.Collections;

public static class ProjectIO {


    public static void SaveVoxbloxProject(string projectFilePath, ref GameObject[] layerList)
    {
        //string projectFilePath = _fileIO.GetProjectSaveFilePath();

        if (projectFilePath != null)
        {

            using (System.IO.StreamWriter outFile = new System.IO.StreamWriter(projectFilePath))
            {
                foreach (GameObject layer in layerList)
                {
                    foreach (Transform block in layer.transform)
                    {
                        outFile.WriteLine(JsonUtility.ToJson(block.GetComponent<VoxelBlock>()));
                    }

                }

            }

        }

    }

    public static void LoadVoxbloxProject(string projectFilePath)
    {

        if (projectFilePath != null)
        {
            using (System.IO.StreamReader inFile = new System.IO.StreamReader(projectFilePath))
            {
                string line;

                while ((line = inFile.ReadLine()) != null)
                {
                    Workspace_Manager.Instance.LoadNewBlock(VoxelBlockJSON.CreateFromJSON(line));
                }

            }

        }

    }

    public static void ExportVoxbloxProject(string projectFilePath, ref GameObject[] layerList)
    {
        // Capture block details and write contents to file

        if (projectFilePath != null)
        {

            // Project Summary Data
            int totalBlockCount = 0;                // Total # of blocks in the model
            int numColors = 10;                     // Total # of different colors

            // Each of the following arrays will kept track of the total number of each block type, seperated into the different colours available
            int[] b1001 = new int[numColors];
            int[] b1002 = new int[numColors];
            int[] b1003 = new int[numColors];
            int[] b1004 = new int[numColors];
            int[] b1005 = new int[numColors];
            int[] b1006 = new int[numColors];
            int[] b1007 = new int[numColors];
            int[] b1008 = new int[numColors];
            int[] b1009 = new int[numColors];
            int[] b1010 = new int[numColors];
            int[] b1011 = new int[numColors];

            // Initialise the arrays;
            for (int i = 0; i < numColors; i++)
            {
                b1001[i] = 0;
                b1002[i] = 0;
                b1003[i] = 0;
                b1004[i] = 0;
                b1005[i] = 0;
                b1006[i] = 0;
                b1007[i] = 0;
                b1008[i] = 0;
                b1009[i] = 0;
                b1010[i] = 0;
                b1011[i] = 0;
            }

            string layerData = null;               // Captures specific data related to the layer for each block during the loop process

            // Analyse block information in each layer
            for (int index = 0; index < layerList.Length; index++)
            {
                // Check there are blocks store in the layer
                if (layerList[index].transform.childCount > 0)
                {

                    // Update output string
                    layerData += "*********************************************************************************\r\n";
                    layerData += "\tLAYER " + index.ToString() + "\r\n";
                    layerData += "*********************************************************************************\r\n";

                    foreach (Transform block in layerList[index].transform)
                    {
                        // Increment total block count
                        totalBlockCount++;

                        // Get the component containing required information
                        VoxelBlock vb = block.GetComponent<VoxelBlock>();

                        // Get the data required
                        int blockID = vb.GetID();
                        Vector3 blockPosition = vb.GetPosition();
                        int blockRotation = vb.GetRotation();
                        string blockColor = vb.GetColor();
                        int colorIndex = ColorSelector.GetColorID(blockColor);

                        // Update the output string
                        layerData += blockID.ToString() + "\t"
                                        + blockColor + "\t"
                                        + blockPosition.x.ToString() + "\t"
                                        + blockPosition.z.ToString() + "\t"
                                        + blockRotation.ToString() + "\r\n";

                        // Update the block counters
                        if (blockID == 1001)
                        {
                            b1001[colorIndex]++;
                        }
                        else if (blockID == 1002)
                        {
                            b1002[colorIndex]++;
                        }
                        else if (blockID == 1003)
                        {
                            b1003[colorIndex]++;
                        }
                        else if (blockID == 1004)
                        {
                            b1004[colorIndex]++;
                        }
                        else if (blockID == 1005)
                        {
                            b1005[colorIndex]++;
                        }
                        else if (blockID == 1006)
                        {
                            b1006[colorIndex]++;
                        }
                        else if (blockID == 1007)
                        {
                            b1007[colorIndex]++;
                        }
                        else if (blockID == 1008)
                        {
                            b1008[colorIndex]++;
                        }
                        else if (blockID == 1009)
                        {
                            b1009[colorIndex]++;
                        }
                        else if (blockID == 1010)
                        {
                            b1010[colorIndex]++;
                        }
                        else if (blockID == 1011)
                        {
                            b1011[colorIndex]++;
                        }

                    }

                }

                // Get block summary details
                string block1001Data = null;

                for (int i = 0; i < numColors; i++)
                {
                    if (b1001[i] != 0)
                    {
                        ColorSelector.CustomColors color = (ColorSelector.CustomColors)i;
                        block1001Data += "\t" + color.ToString() + ": " + b1001[i] + "\r\n";
                    }

                }

                string block1002Data = null;

                for (int i = 0; i < numColors; i++)
                {
                    if (b1002[i] != 0)
                    {
                        ColorSelector.CustomColors color = (ColorSelector.CustomColors)i;
                        block1002Data += "\t" + color.ToString() + ": " + b1002[i] + "\r\n";
                    }

                }

                string block1003Data = null;

                for (int i = 0; i < numColors; i++)
                {
                    if (b1003[i] != 0)
                    {
                        ColorSelector.CustomColors color = (ColorSelector.CustomColors)i;
                        block1003Data += "\t" + color.ToString() + ": " + b1003[i] + "\r\n";
                    }

                }

                string block1004Data = null;

                for (int i = 0; i < numColors; i++)
                {
                    if (b1004[i] != 0)
                    {
                        ColorSelector.CustomColors color = (ColorSelector.CustomColors)i;
                        block1004Data += "\t" + color.ToString() + ": " + b1004[i] + "\r\n";
                    }

                }

                string block1005Data = null;

                for (int i = 0; i < numColors; i++)
                {
                    if (b1005[i] != 0)
                    {
                        ColorSelector.CustomColors color = (ColorSelector.CustomColors)i;
                        block1005Data += "\t" + color.ToString() + ": " + b1005[i] + "\r\n";
                    }

                }

                string block1006Data = null;

                for (int i = 0; i < numColors; i++)
                {
                    if (b1006[i] != 0)
                    {
                        ColorSelector.CustomColors color = (ColorSelector.CustomColors)i;
                        block1006Data += "\t" + color.ToString() + ": " + b1006[i] + "\r\n";
                    }

                }

                string block1007Data = null;

                for (int i = 0; i < numColors; i++)
                {
                    if (b1007[i] != 0)
                    {
                        ColorSelector.CustomColors color = (ColorSelector.CustomColors)i;
                        block1007Data += "\t" + color.ToString() + ": " + b1007[i] + "\r\n";
                    }

                }

                string block1008Data = null;

                for (int i = 0; i < numColors; i++)
                {
                    if (b1008[i] != 0)
                    {
                        ColorSelector.CustomColors color = (ColorSelector.CustomColors)i;
                        block1008Data += "\t" + color.ToString() + ": " + b1008[i] + "\r\n";
                    }

                }

                string block1009Data = null;

                for (int i = 0; i < numColors; i++)
                {
                    if (b1009[i] != 0)
                    {
                        ColorSelector.CustomColors color = (ColorSelector.CustomColors)i;
                        block1009Data += "\t" + color.ToString() + ": " + b1009[i] + "\r\n";
                    }

                }

                string block1010Data = null;

                for (int i = 0; i < numColors; i++)
                {
                    if (b1010[i] != 0)
                    {
                        ColorSelector.CustomColors color = (ColorSelector.CustomColors)i;
                        block1010Data += "\t" + color.ToString() + ": " + b1010[i] + "\r\n";
                    }

                }

                string block1011Data = null;

                for (int i = 0; i < numColors; i++)
                {
                    if (b1011[i] != 0)
                    {
                        ColorSelector.CustomColors color = (ColorSelector.CustomColors)i;
                        block1011Data += "\t" + color.ToString() + ": " + b1011[i] + "\r\n";
                    }

                }

                // Write captured data to file
                using (System.IO.StreamWriter outFile = new System.IO.StreamWriter(projectFilePath))
                {
                    // Project Summary
                    outFile.WriteLine("*********************************************************************************");
                    outFile.WriteLine("    PROJECT SUMMARY                                                              ");
                    outFile.WriteLine("*********************************************************************************");
                    outFile.WriteLine("");
                    outFile.WriteLine("Model height: " + Workspace_Manager.Instance.GetLargetstY().ToString() + " layers");
                    outFile.WriteLine("Total number of blocks used: " + totalBlockCount.ToString());
                    outFile.WriteLine("");

                    // Block Summary
                    outFile.WriteLine("*********************************************************************************");
                    outFile.WriteLine("    BLOCK SUMMARY                                                                ");
                    outFile.WriteLine("*********************************************************************************");
                    outFile.WriteLine("");
                    outFile.WriteLine(" The following details provide a summary for all of the blocks used in the project.");
                    outFile.WriteLine(" With this information, you can tell how many blocks of each color will be required.");
                    outFile.WriteLine("");
                    outFile.WriteLine("*********************************************************************************");
                    outFile.WriteLine("");


                    if (block1001Data != null)
                    {
                        outFile.WriteLine("Block type 1001:");
                        outFile.WriteLine(block1001Data);
                    }

                    if (block1002Data != null)
                    {
                        outFile.WriteLine("Block type 1002:");
                        outFile.WriteLine(block1002Data);
                    }

                    if (block1003Data != null)
                    {
                        outFile.WriteLine("Block type 1003:");
                        outFile.WriteLine(block1003Data);
                    }

                    if (block1004Data != null)
                    {
                        outFile.WriteLine("Block type 1004:");
                        outFile.WriteLine(block1004Data);
                    }

                    if (block1005Data != null)
                    {
                        outFile.WriteLine("Block type 1005:");
                        outFile.WriteLine(block1005Data);
                    }

                    if (block1006Data != null)
                    {
                        outFile.WriteLine("Block type 1006:");
                        outFile.WriteLine(block1006Data);
                    }

                    if (block1007Data != null)
                    {
                        outFile.WriteLine("Block type 1007:");
                        outFile.WriteLine(block1007Data);
                    }

                    if (block1008Data != null)
                    {
                        outFile.WriteLine("Block type 1008:");
                        outFile.WriteLine(block1008Data);
                    }

                    if (block1009Data != null)
                    {
                        outFile.WriteLine("Block type 1009:");
                        outFile.WriteLine(block1009Data);
                    }

                    if (block1010Data != null)
                    {
                        outFile.WriteLine("Block type 1010:");
                        outFile.WriteLine(block1010Data);
                    }

                    if (block1011Data != null)
                    {
                        outFile.WriteLine("Block type 1011:");
                        outFile.WriteLine(block1011Data);
                    }

                    outFile.WriteLine("");
                    outFile.WriteLine("*********************************************************************************");
                    outFile.WriteLine("    LAYER DETAILS                                                                ");
                    outFile.WriteLine("*********************************************************************************");
                    outFile.WriteLine("");
                    outFile.WriteLine(" The following details provide information for where blocks are placed in each layer, ");
                    outFile.WriteLine(" using this format: ");
                    outFile.WriteLine("");
                    outFile.WriteLine(" \tBlockID, Color, PositionX, PositionZ, Rotation");
                    outFile.WriteLine("");
                    outFile.WriteLine(layerData);
                    outFile.WriteLine("*********************************************************************************");

                }

            }

        }

    }

}
