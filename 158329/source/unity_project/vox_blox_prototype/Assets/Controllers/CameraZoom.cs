﻿using UnityEngine;

public class CameraZoom : MonoBehaviour {

    float curZoomPos, zoomTo, zoomOrigin; // curZoomPos is current zoom level - zoomTo is Max zoom in - zoomFrom is Max zoom Out

    // Use this for initialization
    void Start () {
        zoomOrigin = 60.0f;
    }
	
	// Update is called once per frame
	void Update () {
        // Attaches the float y to scrollwheel up or down
        float delta = Input.GetAxis("Mouse ScrollWheel");

        // If the wheel goes up it, decrement 5 from "zoomTo"
        if (delta > 0.0f)
        {
            zoomTo -= 5f;
        }

        // If the wheel goes down, increment 5 to "zoomTo"
        else if (delta < 0.0f)
        {
            zoomTo += 5f;
        }

        // creates a value to raise and lower the camera's field of view
        curZoomPos = zoomOrigin + zoomTo;

        curZoomPos = Mathf.Clamp(curZoomPos, 15.0f, 90.0f);

        // Stops "zoomTo" value at the nearest and farthest zoom value you desire
        zoomTo = Mathf.Clamp(zoomTo, -45.0f, 30.0f);

        // Makes the actual change to Field Of View
        Camera.main.fieldOfView = curZoomPos;
    }
}
