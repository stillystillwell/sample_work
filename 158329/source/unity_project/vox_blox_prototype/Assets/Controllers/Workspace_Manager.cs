﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using vox_blox;

public class Workspace_Manager : MonoBehaviour {

    // WORKSPACE COMPONENTS

    // File Input/Output Components
    FileIO _fileIO;

    // Grid Components
    private Grid3D _workspaceMaskGrid;
    private const int _maxGridSize = 256;
    private int _gridHeight;
    private int _gridWidth;
    private int _gridDepth;
    private float _largestXpos;
    private float _largestYpos;
    private float _largestZpos;

    // Layer Components
    public GameObject _layerHeirachyParent;
    private GameObject[] _layerList;
    private int _currentLayer = 0;
    private bool _displayLayersAbove;
    private bool _displayLayersBelow;

    // Scene Components
    public GameObject _userCameraController;
    private ColorSelector.CustomColors _activeBlockColor = ColorSelector.CustomColors.White;

    // SETUP SINGLETON

    private static Workspace_Manager instance = null;

    public static Workspace_Manager Instance
    {
        get { return instance; }
    }

    private void SetThisManager()
    {
        // This enforces the singleton pattern, meaning there can only ever be one instance of a Manager.
        if (instance == null)

            instance = this;

        else if (instance != null)

            Destroy(this.gameObject);

        DontDestroyOnLoad(this.gameObject);
    }

    // MONOBEHAVIOUR METHODS

    void Awake()
    {
        SetThisManager();
    }

    void Start ()
    {
        InitWorkspace();
	}

	void Update ()
    {
        CheckLayerDisplay();
	}

    // WORKSPACE INITIALIZATION

    private void InitWorkspace()
    {
        _fileIO = new FileIO(Message_Manager.Instance);

        // Build grid system
        _gridHeight = _maxGridSize;
        _gridWidth = _maxGridSize;
        _gridDepth = _maxGridSize;

        _largestXpos = 0;
        _largestYpos = 0;
        _largestZpos = 0;

        _workspaceMaskGrid = new Grid3D(_gridHeight, _gridWidth, _gridDepth);

        // Build layer system
        _displayLayersAbove = true;
        _displayLayersBelow = true;

        _layerList = new GameObject[_maxGridSize];

        for (int i = 0; i < _maxGridSize; i++)
        {
            GameObject tempGO = new GameObject("Layer_" + i.ToString());
            tempGO.transform.parent = _layerHeirachyParent.transform;
            _layerList[i] = tempGO;
        }

    }

    private void ResetWorkspace()
    {
        // Destroy all of the gameobjects in the scene
        for (int i = 0; i < _maxGridSize; i++)
        {
            Destroy(_layerList[i]);
        }

        // Rebuild the workspace
        InitWorkspace();

    }

    // PROJECT MENU FUNCTIONS

    public void SetupNewProject()
    {
        // Rebuild workspace to original state
        ResetWorkspace();
    }

    public void LoadProjectFromFile()
    {
        // Reset current workspace
        ResetWorkspace();

        // Locate saved .voxblox project file
        string projectFilePath = _fileIO.GetProjectOpenFilePath();

        // Build workspace from file
        ProjectIO.LoadVoxbloxProject(projectFilePath);
    }

    public void SaveProjectToFile()
    {
        // Get a .voxblox file to save to
        string projectFilePath = _fileIO.GetProjectSaveFilePath();

        // Save workspace contents to file
        ProjectIO.SaveVoxbloxProject(projectFilePath, ref _layerList);
    }

    public void ExportProjectData()
    {
        // Get a .txt file to output instuction information
        string projectFilePath = _fileIO.GetProjectExportFilePath();

        // Export workspace details to file
        ProjectIO.ExportVoxbloxProject(projectFilePath, ref _layerList);
    }

    // LAYER DISPLAY AND POSITION

    private void CheckLayerDisplay()
    {
        if (!_displayLayersAbove)
        {
            for (int i = _currentLayer + 1; i < _maxGridSize; i++)
            {
                _layerList[i].SetActive(false);
            }

        }
        else
        {
            for (int i = _currentLayer + 1; i < _maxGridSize; i++)
            {
                _layerList[i].SetActive(true);
            }
        }

        if (!_displayLayersBelow)
        {
            for (int i = _currentLayer - 1; i >= 0; i--)
            {
                _layerList[i].SetActive(false);
            }

        }
        else
        {
            for (int i = _currentLayer - 1; i >= 0; i--)
            {
                _layerList[i].SetActive(true);
            }
        }

        _layerList[_currentLayer].SetActive(true);

    }

    public void ToggleViewLayersAbove()
    {
        _displayLayersAbove = !_displayLayersAbove;
    }

    public void ToggleViewLayersBelow()
    {
        _displayLayersBelow = !_displayLayersBelow;
    }

    public void MoveWorkspaceLayerUp()
    {

        if (_currentLayer < _maxGridSize - 1)
        {
            _currentLayer++;
            this.GetComponent<Workspace_Grid>().moveWorkspaceUp();
        }
        
    }

    public void MoveWorkspaceLayerDown()
    {

        if (_currentLayer > 0)
        {
            _currentLayer--;
            this.GetComponent<Workspace_Grid>().moveWorkspaceDown();

        }

    }

    public float GetCurrentLayer()
    {
        return _currentLayer;
    }

    public string GetCurrentLayerName()
    {
        return "Layer_" + _currentLayer.ToString();
    }

    public float GetLargetstX()
    {
        return _largestXpos;
    }

    public float GetLargetstY()
    {
        return _largestYpos;
    }

    public float GetLargestZ()
    {
        return _largestZpos;
    }

    // COLOR MANAGEMENT

    public void SetActiveBlockColor(ColorSelector.CustomColors c)
    {
        _activeBlockColor = c;
    }

    public ColorSelector.CustomColors GetActiveBlockColor()
    {
        return _activeBlockColor;
    }

    // ADDING BLOCKS

    public void AddNewBlock(GameObject block)
    {
        // Get components details
        VoxelBlock vb = block.GetComponent<VoxelBlock>();

        string blockId = vb.GetID().ToString();
        Vector3 blockPosition = vb.GetPosition();
        Quaternion blockRotation = block.transform.rotation;

        // Add to scene
        GameObject tempGO = (GameObject)GameObject.Instantiate(Resources.Load(blockId), blockPosition, blockRotation);

        // Update the voxelblock data
        tempGO.GetComponent<VoxelBlock>().UpdatePostion();
        tempGO.GetComponent<VoxelBlock>().UpdateRotation();

        // Update color
        tempGO.GetComponent<Renderer>().material.color = ColorSelector.GetColor(GetActiveBlockColor());
        tempGO.GetComponent<VoxelBlock>().SetColor(_activeBlockColor.ToString());

        // Update the position in the heirachy
        tempGO.transform.parent = _layerList[_currentLayer].transform;

        // Check new position and update stored variables for largest values accordingly
        int newXpos = (int)tempGO.transform.position.x;
        if (newXpos > _largestXpos)
        {
            _largestXpos = newXpos;
        }
        int newZpos = (int)tempGO.transform.position.z;
        if (newZpos > _largestZpos)
        {
            _largestZpos = newZpos;
        }
        int newYpos = (int)tempGO.transform.position.y;
        if (newYpos > _largestYpos)
        {
            _largestYpos = newYpos;
        }

        // Add block mask to workspace mask
        AddBlockMaskToGrid(tempGO);

    }

    public void LoadNewBlock(VoxelBlockJSON data)
    {
        string blockId = data._id.ToString();
        Vector3 blockPosition = new Vector3(data._xPos, data._yPos, data._zPos);
        Quaternion blockRotation = Quaternion.identity * Quaternion.AngleAxis(data._rotation, Vector3.up);
        string blockColor = data._color;

        // Add to scene
        GameObject tempGO = (GameObject)GameObject.Instantiate(Resources.Load(blockId), blockPosition, blockRotation);

        // Update the voxelblock data
        tempGO.GetComponent<VoxelBlock>().UpdatePostion();
        tempGO.GetComponent<VoxelBlock>().UpdateRotation();

        // Update color
        tempGO.GetComponent<Renderer>().material.color = ColorSelector.GetColor(blockColor);
        tempGO.GetComponent<VoxelBlock>().SetColor(blockColor);

        // Update the position in the heirachy
        tempGO.transform.parent = _layerList[(int)blockPosition.y].transform;

        // Check new position and update stored variables for largest values accordingly
        int newXpos = (int)tempGO.transform.position.x;
        if (newXpos > _largestXpos)
        {
            _largestXpos = newXpos;
        }
        int newZpos = (int)tempGO.transform.position.z;
        if (newZpos > _largestZpos)
        {
            _largestZpos = newZpos;
        }
        int newYpos = (int)tempGO.transform.position.y;
        if (newYpos > _largestYpos)
        {
            _largestYpos = newYpos;
        }

        // Add block mask to workspace mask
        AddBlockMaskToGrid(tempGO);
    }

    public void BuildModelFromImportList(List<Vox2BloxOutputNode>[] blockList)
    {

        for (int i = 0; i < blockList.Length; i++)
        {
            foreach(Vox2BloxOutputNode block in blockList[i])
            {
                // Get the data required to build a new gameobject
                string blockID = block.GetBlockID().ToString();
                Vector3 blockPosition = new Vector3(block.GetXPosition(), i, block.GetYPosition());
                int blockRotateVal = block.GetRotation();
                Quaternion blockRotate = Quaternion.identity * Quaternion.AngleAxis(blockRotateVal, Vector3.up);

                // Instantiate the prefab associated with the block ID
                GameObject tempGO = (GameObject)GameObject.Instantiate(Resources.Load(blockID), blockPosition, blockRotate);

                // Calculate offset the orientation to account for variances in search output combined with the prefab rotation
                Quaternion updateRotate = Quaternion.identity;
                int xOffset = 0;
                int zOffset = 0;
                Vector3 updatePosition = new Vector3();

                // Output from search algorithm varies for even and odd layers
                if (i % 2 == 0)
                {

                    if (blockRotateVal == 0)
                    {
                        updateRotate *= Quaternion.AngleAxis(90.0f, Vector3.up);
                        zOffset = (int)tempGO.GetComponent<VoxelBlock>().GetDimensions().x - 1;
                    }
                    else if (blockRotateVal == 90)
                    {
                        updateRotate *= Quaternion.AngleAxis(270.0f, Vector3.up);
                    }

                }
                else
                {

                    if (blockRotateVal == 0)
                    {
                        updateRotate *= Quaternion.AngleAxis(90.0f, Vector3.up);
                        xOffset = 1 - (int)tempGO.GetComponent<VoxelBlock>().GetDimensions().z;
                    }
                    else if (blockRotateVal == 90)
                    {
                        updateRotate *= Quaternion.AngleAxis(90.0f, Vector3.up);
                    }

                }

                // Apply the offset
                tempGO.transform.rotation *= updateRotate;

                updatePosition = new Vector3(xOffset, 0, zOffset);
                tempGO.transform.position += updatePosition;

                // Update the voxelblock data
                tempGO.GetComponent<VoxelBlock>().UpdatePostion();
                tempGO.GetComponent<VoxelBlock>().UpdateRotation();

                // Check the block can be placed in the scene
                // This enables multiple imports into on project
                if (CanPlaceBlock(tempGO.transform))
                {
                    // Add the block GameObject to the correct layer in the heirachy
                    tempGO.transform.parent = _layerList[i].transform;

                    // Update the color
                    tempGO.GetComponent<Renderer>().material.color = ColorSelector.GetColor(GetActiveBlockColor());
                    tempGO.GetComponent<VoxelBlock>().SetColor(_activeBlockColor.ToString());

                    // Check new position and update stored variables for largest values accordingly
                    int newXpos = (int)tempGO.transform.position.x;
                    if (newXpos > _largestXpos)
                    {
                        _largestXpos = newXpos;
                    }
                    int newZpos = (int)tempGO.transform.position.z;
                    if (newZpos > _largestZpos)
                    {
                        _largestZpos = newZpos;
                    }
                    int newYpos = (int)tempGO.transform.position.y;
                    if (newYpos > _largestYpos)
                    {
                        _largestYpos = newYpos;
                    }

                    // Add block mask to workspace mask
                    AddBlockMaskToGrid(tempGO);
                }
                else
                {
                    Destroy(tempGO);
                }

            }

        }

    }

    private void AddBlockMaskToGrid(GameObject GO)
    {
        // Get mask information
        VoxelBlock vb = GO.GetComponent<VoxelBlock>();
        Grid3D blockMask = vb.GetBlockMask();

        // Get starting position
        int gridPosX = (int)vb.GetPosition().x;
        int gridPosY = (int)vb.GetPosition().y;
        int gridPosZ = (int)vb.GetPosition().z;

        // Offset starting position to accomodate rotation
        int blockRotation = vb.GetRotation();
        Vector3 blockDimensions = vb.GetDimensions();

        if (blockRotation == 0)
        {
            // No action required
        }
        else if (blockRotation == 90)
        {
            gridPosZ -= ((int)blockDimensions.x - 1);
        }
        else if (blockRotation == 180)
        {
            gridPosX -= ((int)blockDimensions.x - 1);
            gridPosZ -= ((int)blockDimensions.z - 1);
        }
        else if (blockRotation == 270)
        {
            gridPosX -= ((int)blockDimensions.z - 1);
        }

        // Add block mask to workspace grid mask
        for (int i = 0; i < blockMask.GetHeight(); i++)
        {
            for (int j = 0; j < blockMask.GetWidth(); j++)
            {
                for (int k = 0; k < blockMask.GetDepth(); k++)
                {
                    if (blockMask.GetElement(i, j, k) == 1)
                    {
                        // This element in the block mask is occupied
                        // Add this element to the workspace grid
                        _workspaceMaskGrid.SetElement(gridPosY + i, gridPosX + j, gridPosZ + k, 1);

                    }

                }

            }

        }

    }

    public bool CanPlaceBlock(Transform T)
    {
        if (((T.position.x >= 0) && (T.transform.position.z >= 0)) && ((T.transform.position.x <= 255) && (T.transform.position.z <= 255)))
        {
            // Cursor is within bounds
            // Check the voxel positions in the block mask are not already occupied in the workspace mask

            // Get mask information
            VoxelBlock vb = T.GetComponent<VoxelBlock>();
            Grid3D blockMask = vb.GetBlockMask();

            // Get starting position
            int gridPosX = (int)vb.GetPosition().x;
            int gridPosY = (int)vb.GetPosition().y;
            int gridPosZ = (int)vb.GetPosition().z;

            // Offset starting position to accomodate rotation
            int blockRotation = vb.GetRotation();
            Vector3 blockDimensions = vb.GetDimensions();

            if (blockRotation == 0)
            {
                // No action required
            }
            else if (blockRotation == 90)
            {
                gridPosZ -= ((int)blockDimensions.x - 1);
            }
            else if (blockRotation == 180)
            {
                gridPosX -= ((int)blockDimensions.x - 1);
                gridPosZ -= ((int)blockDimensions.z - 1);
            }
            else if (blockRotation == 270)
            {
                gridPosX -= ((int)blockDimensions.z - 1);
            }

            // Cross reference elements in the block mask against workspace mask
            for (int i = 0; i < blockMask.GetHeight(); i++)
            {
                for (int j = 0; j < blockMask.GetWidth(); j++)
                {
                    for (int k = 0; k < blockMask.GetDepth(); k++)
                    {
                        if (blockMask.GetElement(i, j, k) == 1)
                        {
                            // This element in the block mask is occupied
                            // Check the associted element in the workspace mask is not already occupied

                            try
                            {
                                if (_workspaceMaskGrid.GetElement(gridPosY + i, gridPosX + j, gridPosZ + k) != 0)
                                {
                                    // This space is already occupied
                                    return false;
                                }

                            }
                            catch (System.IndexOutOfRangeException)
                            {
                                // Index is out of bounds
                                return false;

                            }

                        }

                    }

                }

            }

            // All checks passed successfully
            return true;

        }
        else
        {
            // Block is out of bounds
            return false;
        }

    }

    // DELETING BLOCKS

    public void DeleteBlock(GameObject block)
    {
        // TODO: Remove block mask from layer mask
        DeleteBlockMaskFromGrid(block);
        // Delete this GameObject
        Destroy(block);
    }


    private void DeleteBlockMaskFromGrid(GameObject GO)
    {
        // Get mask information
        VoxelBlock vb = GO.GetComponent<VoxelBlock>();
        Grid3D blockMask = vb.GetBlockMask();

        // Get starting position
        int gridPosX = (int)vb.GetPosition().x;
        int gridPosY = (int)vb.GetPosition().y;
        int gridPosZ = (int)vb.GetPosition().z;

        // Offset starting position to accomodate rotation
        int blockRotation = vb.GetRotation();
        Vector3 blockDimensions = vb.GetDimensions();

        if (blockRotation == 0)
        {
            // No action required
        }
        else if (blockRotation == 90)
        {
            gridPosZ -= ((int)blockDimensions.x - 1);
        }
        else if (blockRotation == 180)
        {
            gridPosX -= ((int)blockDimensions.x - 1);
            gridPosZ -= ((int)blockDimensions.z - 1);
        }
        else if (blockRotation == 270)
        {
            gridPosX -= ((int)blockDimensions.z - 1);
        }

        // Remove block mask from workspace mask
        for (int i = 0; i < blockMask.GetHeight(); i++)
        {
            for (int j = 0; j < blockMask.GetWidth(); j++)
            {
                for (int k = 0; k < blockMask.GetDepth(); k++)
                {
                    if (blockMask.GetElement(i, j, k) == 1)
                    {
                        // This element in the block mask is occupied
                        // Remove this element from the workspace mask
                        _workspaceMaskGrid.SetElement(gridPosY + i, gridPosX + j, gridPosZ + k, 0);

                    }

                }

            }

        }

    }
    
}
