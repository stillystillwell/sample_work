﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using vox_blox;
using System;

public class Message_Manager : MonoBehaviour, ISubject, IObserver {

    // MESSAGE MANAGER COMPONENTS

    private List<IObserver> _messageObservers = new List<IObserver>();
    private Message _message;

    // SINGELTON SETUP

    private static Message_Manager instance = null;

    public static Message_Manager Instance
    {
        get { return instance; }
    }

    private void SetThisManager()
    {
        // This enforces the singleton pattern, meaning there can only ever be one instance of a Manager.
        if (instance == null)

            instance = this;

        else if (instance != null)

            Destroy(this.gameObject);

        DontDestroyOnLoad(this.gameObject);
    }

    // MONOBEHAVIOUR METHODS

    void Awake()
    {
        SetThisManager();
        RegisterObserver(Message_Manager.Instance);
    }

    void Start () {
        
	}
	
	void Update () {
	
	}

    // MESSAGE PASSING INTERFACE IMPLEMENTATION

    // IObserver Interface Functions

    public void RegisterObserver(IObserver o)
    {
        // Add an instantiated object as an observer for messages
        if(!_messageObservers.Contains(o)) _messageObservers.Add(o);
    }

    public void RemoveObserver(IObserver o)
    {
        // Remove an instantiated object as an observer for messages
        if (_messageObservers.Contains(o)) _messageObservers.Remove(o);
    }

    public void NotifyObservers()
    {
        // Notify each observer that a new message is available
        foreach (var obs in _messageObservers) obs.UpdateMessage(_message);
    }

    public void UpdateMessage(Message m)
    {
        // Action to take when an observer is notified of a new message
        Debug.Log("Message received: " + m.getTitle() + " | " + m.getContent());
        UI_Manager.Instance.UpdateMessageWindowText(m);
    }

    // ISubject Interface Functions

    public void SendMessage(Message m)
    {
        // Store details of the message to be sent and notify the observers
        if (_messageObservers.Count != 0)
        {
            _message = m;
            NotifyObservers();
        }

    }

}
