﻿using UnityEngine;
using System.Collections.Generic;

public class Workspace_Grid : MonoBehaviour
{

    // Object positions
    public GameObject groundLayer;
    public GameObject currentLayer;
    public GameObject sceneCollider;
    private List<GameObject> _groundList;
    private List<GameObject> _currentList;
    private Transform _gridAnchor;
    private Vector3 _gridCoord, _currentLCoord;

    // Size of to workplatform
    // public float currentY = -0.5f;
    public float currentY = 0.0f;
    private float xScale = 64.0f;
    private float zScale = 64.0f;
    private float _oldScale_X = -1.0f;
    private float _oldScale_Z = -1.0f;
    private float _oldCurrentY;


    // Use this for initialization
    void Start()
    {
        // BaseGrid Init
        _groundList = new List<GameObject>();
        _currentList = new List<GameObject>();
        _gridAnchor = groundLayer.transform;
        _gridCoord.x = 0.0f;
        //_gridCoord.y = -0.5f;
        _gridCoord.y = 0.0f;
        _gridCoord.z = 0.0f;

        _currentLCoord = _gridCoord;
        //_currentLCoord.y = currentY - 0.5f;
        _currentLCoord.y = currentY;
        _gridAnchor.position = _gridCoord;

        _oldCurrentY = currentY;

        sceneCollider = Instantiate(sceneCollider, _currentLCoord, Quaternion.identity) as GameObject;
        sceneCollider.transform.parent = GameObject.Find("Grid_System").transform;

    }

    // Update is called once per frame
    void Update()
    {
        // Checks to see if the workspace has expanded and rebuilds the workspace grid accordingly
        xScale = Workspace_Manager.Instance.GetLargetstX();
        zScale = Workspace_Manager.Instance.GetLargestZ();

        // Increase Grid Size
        if ((xScale != _oldScale_X) || (zScale != _oldScale_Z) || (currentY != _oldCurrentY))
        {
            // Clear Un-used tiles
            clearWorkspace();

            // Build Grid System
            buildWorkspace();

            // Set Old to new values
            setNewScales();
        }

    }

    /// <summary>
    /// Clears Worksspace of un-needed space
    /// </summary>
    private void clearWorkspace()
    {
        if ((xScale < _oldScale_X) || (zScale < _oldScale_Z))
        {
            // Clear Ground
            foreach (GameObject g in _groundList)
            {
                Vector3 tempVect = g.transform.position;
                if ((tempVect.x > xScale) || (tempVect.z > zScale))
                {
                    _groundList.Remove(g);
                    Destroy(g);
                }

            }
            // Clear Current Workspace
            foreach (GameObject g in _currentList)
            {
                Vector3 tempVect = g.transform.position;
                if ((tempVect.x > xScale) || (tempVect.z > zScale) || (currentY != _oldCurrentY))
                {
                    _currentList.Remove(g);
                    Destroy(g);
                }

            }

        }
    }

    /// <summary>
    /// Builds full workspace based on largest scale
    /// </summary>
    private void buildWorkspace()
    {
        // Build Z grid
        for (float i = 0.0f; i <= zScale; i += 64.0f)
        {
            _gridCoord.z = i;
            // Build X grid
            for (float j = 0.0f; j <= xScale; j += 64.0f)
            {
                bool add_Object = true;

                _gridCoord.x = j;
                _gridAnchor.position = _gridCoord;
                foreach (GameObject g in _groundList)
                {
                    if (g.transform.position == _gridAnchor.position)
                    {
                        add_Object = false;
                    }

                }
                if (add_Object)
                {
                    workspaceScaleUp();

                }
                if (currentY != _oldCurrentY)
                {
                    workpacePositionChange();
                }

            }


        }
    }

    /// <summary>
    /// Updates old values
    /// </summary>
    private void setNewScales()
    {
        _oldScale_X = xScale;
        _oldScale_Z = zScale;
        _oldCurrentY = currentY;
    }

    /// <summary>
    /// Moves Workspace Up/Down
    /// </summary>
    private void workpacePositionChange()
    {
        foreach (GameObject g in _currentList)
        {
            Transform currPos = g.transform;
            Vector3 tempTCurrent;
            tempTCurrent.x = g.transform.position.x;
            // tempTCurrent.y = currentY - 0.5f;
            tempTCurrent.y = currentY;
            tempTCurrent.z = g.transform.position.z;
            currPos.position = tempTCurrent;
            g.transform.position = currPos.position;
        }

        Vector3 colliderPos = sceneCollider.transform.position;
        //colliderPos.y = currentY - 0.5f;
        colliderPos.y = currentY - 0.5f;
        sceneCollider.transform.position = colliderPos;

    }

    /// <summary>
    /// Scales workspace based on largest size
    /// </summary>
    private void workspaceScaleUp()
    {
        GameObject tempObjectF, tempObjectC;


        // Ground
        tempObjectF = Instantiate(groundLayer, _gridAnchor.position, _gridAnchor.rotation) as GameObject;
        tempObjectF.SetActive(true);
        _groundList.Add(tempObjectF);

        tempObjectF.transform.parent = GameObject.Find("Grid_System").transform;

        // Current
        Transform tempT = _gridAnchor;
        _currentLCoord = _gridCoord;
        //_currentLCoord.y = currentY - 0.5f;
        _currentLCoord.y = currentY;
        tempT.position = _currentLCoord;
        tempObjectC = Instantiate(currentLayer, tempT.position, tempT.rotation) as GameObject;
        tempObjectC.SetActive(true);
        _currentList.Add(tempObjectC);

        tempObjectC.transform.parent = GameObject.Find("Grid_System").transform;
    }


    /// <summary>
    /// Moves The Camera Up with the Current Layer
    /// </summary>
    public void moveWorkspaceUp()
    {
        Transform camera = GameObject.Find("FPS_Controller").transform;
        if (currentY <= 256)
        {
            currentY++;
            Vector3 newPos = camera.position;
            newPos.y++;
            camera.transform.position = newPos;
        }
    }

    /// <summary>
    /// Moves the Camear Down with the Current Layer
    /// </summary>
    public void moveWorkspaceDown()
    {
        Transform camera = GameObject.Find("FPS_Controller").transform;
        if (currentY >= 1)
        {
            currentY--;
            Vector3 newPos = camera.position;
            newPos.y--;
            camera.transform.position = newPos;
        }
    }

    /// <summary>
    /// Sets Current layer based on input from interface
    /// </summary>
    /// <param name="layerPos"></param>
    public void setLayer(int layerPos)
    {
        currentY = layerPos;
        Transform camera = GameObject.Find("FPSController").transform;
        Vector3 newPos = camera.position;
        newPos.y = currentY + 5.0f;
        camera.transform.position = newPos;
    }

    public void ToggleGridVisibility()
    {
        foreach(GameObject GO in _currentList)
        {
            GO.SetActive(!GO.activeInHierarchy);
        }
        
    }

}
