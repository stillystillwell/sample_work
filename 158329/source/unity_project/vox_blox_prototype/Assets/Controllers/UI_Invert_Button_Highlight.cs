﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using System.Collections;
using System;

public class UI_Invert_Button_Highlight : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    public Button btn;
    public Text btnTxt;
    public Color stdBlue;

    public void OnPointerEnter(PointerEventData eventData)
    {
        btn.image.color = stdBlue;
        btnTxt.color = Color.white;
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        btn.image.color = Color.white;
        btnTxt.color = stdBlue;
    }
}
