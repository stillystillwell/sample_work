﻿using UnityEngine;
using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using vox_blox;

public class Import_Manager : MonoBehaviour {

    // IMPORT MANAGER COMPONENTS

    // File and directory management components
    private FileIO _fileIO;                                 // Manages IO for original model used to make a copy
    private string _projectDirectory;                       // Main directory of the project
    private string _resourceDirectory;                      // Directory of the Resources folder
    private string _binvoxDirectory;                        // Directory of executable Binvox programs used for conversion
    private string _tempModelDataDirectory;                 // Directory used to store manipulated model files temporarily
    
     
    // Imported model components
    private FileInfo    _originalModelFileInfo;             // File information of the original model
    private FileInfo    _importedModelFileInfo;             // File information of the copied model
    private GameObject  _importedModelObject = null;        // Imported model built at run time
    private Grid3D      _importedModelVoxelMap;             // Stores voxel map for an imported 3D model
    public GameObject   _importedModelBoundaryBox;          // Boundary box the imported model is scaled to fit within
    public GameObject   _rotationReferenceAxis;             // Prefab model used as a visual reference for the orientation of the imported model
    public Material     _importedModelMaterial;             // Default material applied to the imported 

    // Import settings
    private float       _defaultRotation = 10.0f;           // Rotation (in radians) to be applied when manipulating the imported model
    private bool        _hollowModel = true;                // For toggling hollowing of imported model during voxelization

    // SINGELTON SETUP 

    private static Import_Manager instance = null;

    public static Import_Manager Instance
    {
        get { return instance; }
    }

    private void SetThisManager()
    {
        // This enforces the singleton pattern, meaning there can only ever be one instance of a Manager.
        if (instance == null)

            instance = this;
        
        else if (instance != null)

            Destroy(this.gameObject);  
        
        DontDestroyOnLoad(this.gameObject);
    }

    // MONOBEHAVIOUR METHODS

    void Awake()
    {
        SetThisManager();
    }
	void Start()
    {
        // Setup directories
        // Create new FileIO object and pass across message receiver
        _fileIO = new FileIO(Message_Manager.Instance);
        // Get the directory of the project folder
        _projectDirectory = Directory.GetCurrentDirectory();
        // Get the resource directory
        _resourceDirectory = _fileIO.DwnDir(_projectDirectory, new string[] { "Assets", "Resources" });
        // Get the directory of the binvox folder
        _binvoxDirectory = _fileIO.DwnDir(_projectDirectory, new string[] { "Assets", "Import", "binvox" });
        // Get the directory of the temp model data folder
        _tempModelDataDirectory = _fileIO.DwnDir(_projectDirectory, new string[] { "Assets", "Import", "temp_model_data" });
        // Initialise default settings
        ResetModelImport();
    }

    void Update()
    {
        
	}

    // PUBLIC FUNCTIONS

    // Main Import Actions

    public void SelectObjFile()
    {
        // Open file select dialog box and get file info of the selected file
        FileInfo inFile = _fileIO.GetObjFileInfo();
        if(inFile != null)
        {
            StartCoroutine(LoadModel(inFile));
        }
    }

    public void ProcessModelImport()
    {
        if (_importedModelObject != null)
        {
            _hollowModel = UI_Manager.Instance.HollowModelIsChecked();
            StartCoroutine(ModelImportCoroutine());
            //ProcessImportNonThreaded();
        }
        else
        {
            // Error. There is no model
            Debug.Log("ProcessModelImport(): No model found");
            CancelModelImport();
        }
        
    }

    public void CancelModelImport()
    {
        ResetModelImport();
        UI_Manager.Instance.SetDefaultUI();
    }

    public void ResetModelImport()
    {
        // Reset any variables associated with a previously imported .obj file
        //if (_importedModelObject != null)
        {
            Destroy(_importedModelObject);                          // Remove the GameObject associated with the previously imported model
            _originalModelFileInfo = null;                          // Remove the reference to the original .obj file
            _importedModelFileInfo = null;                          // Remove the reference to the imported .obj file
            _importedModelVoxelMap = null;                          // Remove the reference any previously created voxelmap
            _fileIO.DeleteAllFiles(_tempModelDataDirectory);        // Delete all files stored in "temp_model_data" directory
            UI_Manager.Instance.ResetFilenameText();                // Reset the filename text details
            SetModelColor(ColorSelector.CustomColors.White);        // Reset the model material color
        }
    }

    // Imported Model Transformation Controls

    public void IncreaseModelRoll()     
    {
        if (_importedModelObject != null)
            _importedModelObject.transform.rotation *= Quaternion.AngleAxis(_defaultRotation, Vector3.forward);
    }

    public void DecreaseModelRoll()
    {
        if (_importedModelObject != null)
            _importedModelObject.transform.rotation *= Quaternion.AngleAxis(-_defaultRotation, Vector3.forward);
    }

    public void IncreaseModelPitch()
    {
        if (_importedModelObject != null)
            _importedModelObject.transform.rotation *= Quaternion.AngleAxis(_defaultRotation, Vector3.right);
    }

    public void DecreaseModelPitch()
    {
        if (_importedModelObject != null)
            _importedModelObject.transform.rotation *= Quaternion.AngleAxis(-_defaultRotation, Vector3.right);
    }

    public void IncreaseModelYaw()
    {
        if (_importedModelObject != null)
            _importedModelObject.transform.rotation *= Quaternion.AngleAxis(_defaultRotation, Vector3.up);
    }

    public void DecreaseModelYaw()
    {
        if (_importedModelObject != null)
            _importedModelObject.transform.rotation *= Quaternion.AngleAxis(-_defaultRotation, Vector3.up);
    }

    public void ResetModelRotation()
    {
        if (_importedModelObject != null)
            _importedModelObject.transform.rotation = Quaternion.identity;
    }

    public void SetModelColor(ColorSelector.CustomColors c)
    {
        _importedModelMaterial.color = ColorSelector.GetColor(c);
    }

    // PRIVATE FUNCTIONS

    private IEnumerator LoadModel(FileInfo originalFile)
    {
        // Display message window
        UI_Manager.Instance.ActivateMessageWindow();
        Message_Manager.Instance.SendMessage(new Message("loading model", "building mesh, please wait"));
        // Pause process and allow Unity to update
        yield return null;
        // Destroy any previously created imported models
        ResetModelImport();
        // Store the details of the original model file
        _originalModelFileInfo = originalFile;
        // Copy the requested .obj file to the import manager folder
        // Prevents the user from modifying the file while we are working with it
        _fileIO.CopyFile(_originalModelFileInfo.FullName, _tempModelDataDirectory);
        // Store the details of the copied model file
        // This is the file we will manipulate during the import process
        _importedModelFileInfo = new FileInfo(Path.Combine(_tempModelDataDirectory, _originalModelFileInfo.Name));
        // Update the information in the UI
        UI_Manager.Instance.SetFilenameText(_importedModelFileInfo.Name);
        // Create a GameObject from the file and place it in the scene
        BuildGameObjectFromFile();
        // Process complete
        // Deactivate message window
        UI_Manager.Instance.DeactivateMessageWindow();
        // End of coroutine
        yield return null;
    }

    private void BuildGameObjectFromFile()
    {
        // Builds a game object from a .obj file at run time
        // Resources:
        // https://docs.unity3d.com/Manual/InstantiatingPrefabs.html
        // http://answers.unity3d.com/questions/401675/import-a-3d-model-in-game-during-runtime-in-gui.html#answer-401686
        try
        {   
            // Create mesh builder
            OBJ_Importer obj_importer = new OBJ_Importer();
            // Build mesh
            Mesh importedMesh = obj_importer.ImportFile(_importedModelFileInfo.FullName);
            // Create a temporary empty Unity GameObject
            _importedModelObject = new GameObject("temp_imported_model");
            // Add the model as a child to the Boundary Box gameobject
            _importedModelObject.transform.parent = _importedModelBoundaryBox.transform;
            // Add mesh components to GameObject
            MeshFilter prefabMeshFilter = _importedModelObject.AddComponent<MeshFilter>();
            prefabMeshFilter.sharedMesh = importedMesh;
            MeshRenderer prefabMeshRenderer = _importedModelObject.AddComponent<MeshRenderer>();
            prefabMeshRenderer.material = _importedModelMaterial;
            // Position GameObject in scene
            _importedModelObject.transform.position = new Vector3(0, -1000, 0);
            // Scale GameObject
            ScaleImportedModel();
            // Invert X axis to fix mirror effect
            Transform T = _importedModelObject.transform;
            _importedModelObject.transform.localScale = new Vector3(-T.localScale.x, T.localScale.y, T.localScale.z);
            // Make a copy of the reference axis and attach it to the imported object
            GameObject axis = Instantiate(_rotationReferenceAxis);
            axis.transform.parent = _importedModelObject.transform;
        }
        catch (Exception e)
        {
            Debug.Log("Error building game object");
            Debug.Log(e.Message);
        }

    }

    private void ScaleImportedModel()
    {
        // Scales an imported model to the size of a boundary box, which the model is placed within
        // This boundary box has been setup for optimal viewing in the associated camera
        if (_importedModelObject != null)
        {
            // Get the size of the model
            Vector3 importedModelSize = _importedModelObject.GetComponent<MeshFilter>().mesh.bounds.size;
            // Get the size of the boundary box
            Vector3 boundaryBoxSize = _importedModelBoundaryBox.GetComponent<MeshRenderer>().bounds.size;
            // Find maximum length of the imported model
            float maxSize = Math.Max(importedModelSize.x, Math.Max(importedModelSize.y, importedModelSize.z));
            // Calculate scale factor
            float scalefactor = boundaryBoxSize.x / maxSize;
            // Apply scale to imported model
            _importedModelObject.transform.localScale *= scalefactor;
        }

    }

    private IEnumerator ModelImportCoroutine()
    {
        // INITIATE IMPORT PROCESS

        // Hide the import manager UI
        UI_Manager.Instance.DeactivateModelImportMenu();
        // Open message window - messages will be passed to this window during the process to provide feedback to the user
        UI_Manager.Instance.ActivateMessageWindow();
        // Update UI
        Message_Manager.Instance.SendMessage(new Message("processing import", "initiated"));
        yield return null;

        // Create a timer for recording process times
        System.Diagnostics.Stopwatch watch = new System.Diagnostics.Stopwatch();

        // APPLY TRANSFORMATIONS TO OBJ FILE (IF REQUIRED)

        if (_importedModelObject.transform.rotation != Quaternion.identity)
        {
            // Update UI
            Message_Manager.Instance.SendMessage(new Message("processing import", "applying transformations to selected file"));
            yield return null;

            // Parse the obj file associated with the imported model, and apply rotational transformations to the vertices
            // Transformed data is written back to the same file

            ApplyTransformationToFile();

            // Update UI
            Message_Manager.Instance.SendMessage(new Message("processing import", "transformations applied"));
            yield return null;
        }

        // VOXELIZE OBJ FILE

        // Update UI
        Message_Manager.Instance.SendMessage(new Message("processing import", "voxelizing model"));
        yield return null;

        // Get the full filename (including path) to the imported model obj file, without the extension
        string fileNameNoExt = Path.Combine(_tempModelDataDirectory, Path.GetFileNameWithoutExtension(_importedModelFileInfo.FullName));
        // Get the requested height of the model to be imported
        int numLayers = UI_Manager.Instance.GetLayerHeight();
        // Convert from .obj to .binvox, then from .binvox to .txt - this is handled by external programs (binvox.exe & binvox_to_txt.exe)
        BinvoxConverter.ConvertObjToTxt(_binvoxDirectory, fileNameNoExt, numLayers, _hollowModel);

        // Update UI
        Message_Manager.Instance.SendMessage(new Message("processing import", "voxelization complete"));
        yield return null;

        // LOAD VOXEL DATA

        // Create a timer to record .txt file read time
        watch = new System.Diagnostics.Stopwatch();
        watch.Start();

        // Update UI
        Message_Manager.Instance.SendMessage(new Message("processing import", "loading voxel data"));
        yield return null;

        // Find the file containing the voxel data
        string txtfile = Path.ChangeExtension(_importedModelFileInfo.FullName, ".txt");
        if(File.Exists(txtfile))
        {
            // Build a voxel map using the data contained in the .txt file
            _importedModelVoxelMap = VoxelMapBuilder.ConvertTxtToArray(txtfile);

            if (_importedModelVoxelMap != null)
            {
                // PrintVoxelModelContentsToConsole(_importedModelVoxelMap);

                // Update UI
                Message_Manager.Instance.SendMessage(new Message("processing import", "voxel data loaded"));
                yield return null;
            }
            else
            {
                // There was an error reading from the .txt file
                Debug.Log("Error reading voxel data from: " + txtfile);
                // End this coroutine
                yield break;
            }

        }
        else
        {
            // There was an error during the voxelization process
            Debug.Log("Error during the voxelization process | " + txtfile + " not found");
            // End this coroutine
            yield break;
        }

        // Stop time and record conversion time
        watch.Stop();
        Debug.Log("ConvertTxtToGrid complete. Time elapsed: " + watch.Elapsed.TotalSeconds.ToString());

        // PERFORM VOXEL-TO-BLOCK CONVERSION

        // Update UI
        Message_Manager.Instance.SendMessage(new Message("processing import", "solution search initiated"));
        yield return null;

        // Find the blocktypes data file
        string blockDataFile = _resourceDirectory + "\\blocktypes.voxblox";

        // Create a solution builder, which will convert the voxel map into blocks
        Vox2BloxAStarSearchTree solutionBuilder = new Vox2BloxAStarSearchTree(_importedModelVoxelMap, blockDataFile, Message_Manager.Instance, false);

        // Create a container for the output from the solution builder
        List<Vox2BloxOutputNode>[] outputList = null;

        // Create a timer to record voxel-to-block conversion time
        watch = new System.Diagnostics.Stopwatch();
        watch.Start();

        if ((numLayers <= 100) || ((numLayers <= 150) && (_hollowModel)))
        {
            // This is not a complex model
            // The number of layers is within resonable bounds to process each layer individually
            // This method will enable message passing for each layer, to provide better feedback for the user

            // Instatiate the output container
            outputList = new List<Vox2BloxOutputNode>[_importedModelVoxelMap.GetHeight()];

            // Get the mask from the solution builder
            int[][][] maskCopy = solutionBuilder.GetMask();

            // Find a solution for each layer in the voxel map
            for (int i = 0; i < _importedModelVoxelMap.GetHeight(); i++)
            {
                Message_Manager.Instance.SendMessage(new Message("processing import", "building solution for layer " + i.ToString() + " of " + numLayers.ToString()));
                yield return null;

                solutionBuilder.ConvertLayer(ref outputList, maskCopy, i);
            }

        }
        else
        {
            // This is a complex model
            // The number of layers being converted is high, so the whole model will be converted before the UI is updated
            // This method will not pass messages and cause the progam to become unresponsive for up to 10 minutes 

            // Update UI
            Message_Manager.Instance.SendMessage(new Message("processing import", "building complex solution\n(this can take up to 15 minutes, during which program becomes unresponsive)"));
            yield return null;

            // Perform conversion for all 
            outputList = solutionBuilder.ConvertVoxelToBricks();

        }

        // Stop time and record conversion time
        watch.Stop();
        Debug.Log("ConvertVoxelToBricks complete. Time elapsed: " + watch.Elapsed.TotalSeconds.ToString());

        //Debug.Log(solutionBuilder.PrintConvertedMap());
        //PrintOutputList(outputList);

        // Update UI
        Message_Manager.Instance.SendMessage(new Message("processing import", "solution search complete"));
        yield return null;

        // ADD BLOCKS TO WORKSPACE

        // Update UI
        Message_Manager.Instance.SendMessage(new Message("processing import", "adding blocks to workspace"));
        yield return null;

        Workspace_Manager.Instance.BuildModelFromImportList(outputList);

        // Update UI
        Message_Manager.Instance.SendMessage(new Message("processing import", "blocks added to workspace successfully"));
        yield return null;

        // IMPORT PROCESS COMPLETE

        // Update UI
        Message_Manager.Instance.SendMessage(new Message("processing import", "complete"));
        yield return null;

        // PROVIDE IMPORT SUMMARY

        // END IMPORT PROCESS

        // Close message window
        UI_Manager.Instance.DeactivateMessageWindow();
        UI_Manager.Instance.SetDefaultUI();
    }

    private void ApplyTransformationToFile()
    {
            try
            {
                // Get the rotation data
                Quaternion q = _importedModelObject.transform.rotation;
                // Apply the transformation to the file
                // Inverted Y & Z to fix rotation issue - unsure why this is...
                ObjFileTransform.TransformObjFile(_importedModelFileInfo.FullName, q.w, q.x, -q.y, -q.z);
            }
            catch (Exception e)
            {
                Debug.Log(e);
            }

    }

    // PRINT METHODS

    public void PrintOutputList(List<Vox2BloxOutputNode>[] list)
    {
        if (list != null)
        {
            int i = 0;
            string printMe = null;

            foreach (List<Vox2BloxOutputNode> layer in list)
            {

                printMe += "Layer " + i.ToString() + "\n";

                foreach (Vox2BloxOutputNode node in layer)
                {
                    printMe += node.GetBlockID() + " " + node.GetXPosition() + " " + node.GetYPosition() + " " + node.GetRotation() + "\n";
                }

                i++;

            }

            Debug.Log("PrintOutputList(): \n" + printMe);   

        }
        else
        {
            Debug.Log("PrintOutputList(): list is empty");
        }
    }

    private void PrintVoxelModelContentsToConsole(Grid3D g)
    {
        Debug.Log("voxelmap generated successfully");

        string output = "";

        for (int i = 0; i < g.GetHeight(); i++)
        {

            int[][] layer = g.GetLayer(i);

            output = output + "layer: " + i.ToString() + "\n";

            for (int j = g.GetDepth() - 1; j >= 0; j--)
            {
                for (int k = 0; k < g.GetWidth(); k++)
                {
                    output = output + layer[j][k].ToString();
                }

                output = output + "\n";

            }

            output = output + "\n";

        }

        Debug.Log(output);

    }

}