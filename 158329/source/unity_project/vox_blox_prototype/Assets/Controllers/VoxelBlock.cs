﻿using UnityEngine;
using System.Collections;
using vox_blox;

[System.Serializable]
public class VoxelBlock : MonoBehaviour {

    // Script attached to prefab blocks to provide certain functionality
    // NB: Most member are kept public so JSON parser can collect information easily

    // MEMBER VARIABLES
    
    // Block Details
    public int _id;
    public int _height;
    public int _width;
    public int _length;
    private Grid3D _blockMask;

    // Scene Details
    public float _xPos;
    public float _yPos;
    public float _zPos;
    public int _rotation;
    public string _color;

    // MONOBEHAVIOUR METHODS

    void Start () {

        // Get position data
        Vector3 position = this.transform.position;
        _xPos = position.x;
        _yPos = position.y;
        _zPos = position.z;

        // Get rotation data
        _rotation = (int)this.transform.eulerAngles.y;

        // Build mask
        BuildBlockMask();

    }
	
	void Update () {
	
	}

    // PUBLIC METHODS

    public int GetID()
    {
        return _id;
    }

    public Vector3 GetDimensions()
    {
        return new Vector3(_width, _height, _length);
    }

    public Vector3 GetPosition()
    {
        return new Vector3(_xPos, _yPos, _zPos);
    }

    public void UpdatePostion()
    {
        Vector3 currentPos = this.transform.position;
        _xPos = currentPos.x;
        _yPos = currentPos.y;
        _zPos = currentPos.z;
    }

    public int GetRotation()
    {
        return _rotation;
    }

    public void UpdateRotation()
    {
        _rotation = (int)this.transform.rotation.eulerAngles.y;

        BuildBlockMask();

    }

    public Grid3D GetBlockMask()
    {
        return _blockMask;
    }

    public string GetColor()
    {
        return _color;
    } 

    public void SetColor(string color)
    {
        if ((color == "Black") || (color == "Grey") || (color == "White") || (color == "Red") || (color == "Orange") || (color == "Yellow") || (color == "Green") || (color == "Blue") || (color == "Purple") || (color == "Pink"))
        {
            _color = color;
        }
        else
        {
            // invalid color
            Debug.Log("VoxelBlock.SetColor() invalid value: " + color);
        }

    }

    // PRIVATE METHODS

    private void BuildBlockMask()
    {
        // Builds a mask, fully populated based dimensions
        // TODO: Update to read block data from file
        // TODO: Update to perform matrix rotation

        if (_rotation == 0)
        {
            _blockMask = new Grid3D(_height, _width, _length);
        }
        else if (_rotation == 90)
        {
            _blockMask = new Grid3D(_height, _length, _width);
        }
        else if (_rotation == 180)
        {
            _blockMask = new Grid3D(_height, _width, _length);
        }
        else if (_rotation == 270)
        {
            _blockMask = new Grid3D(_height, _length, _width);
        }

        _blockMask.SetAllElements(1);

    }

}

public class VoxelBlockJSON
{
    public int _id;
    public int _height;
    public int _width;
    public int _length;
    public float _xPos;
    public float _yPos;
    public float _zPos;
    public int _rotation;
    public string _color;

    public static VoxelBlockJSON CreateFromJSON(string jsonString)
    {
        return JsonUtility.FromJson<VoxelBlockJSON>(jsonString);
    }
}