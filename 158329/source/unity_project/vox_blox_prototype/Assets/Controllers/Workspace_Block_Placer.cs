﻿using UnityEngine;
using System.Collections.Generic;
using vox_blox;

public class Workspace_Block_Placer : MonoBehaviour {

    private Transform _target;
    private List<GameObject> _parent;               // remove this 
    private List<GameObject> _blockList;
    private GameObject _cursor, _block, _layer;
    private Transform _cursorPos;
    //private Color _blockColor;
    private Vector2 _largestScale;
    private string _blockName;
    private int _currentLayer;

    // Use this for initialization
    void Start()
    {
        // Layer Creation
        // Gets curent layer from Grid
        GameObject temp = GameObject.Find("Workspace_Manager");
        _currentLayer = (int)temp.GetComponent<Workspace_Grid>().currentY + 1;
        // Sets Layer
        _layer = new GameObject();
        _layer.name = "Layer" + _currentLayer.ToString();

        // World
        _parent = new List<GameObject>();
        _parent.Add(_layer);
        _blockList = new List<GameObject>();


        // Block Init
        _blockName = "1001";
        // _blockColor = Color_Selector.White;


        // TODO FIND CURSOR_VIEW
        _cursor = GameObject.Find("Workspace_Manager");

        // Init Scene Scale
        _largestScale = new Vector2(1.0f, 1.0f);
    }

    void Update()
    {
        _blockName = _cursor.GetComponent<Workspace_Cursor>().blockName();
        _cursorPos = _cursor.GetComponent<Workspace_Cursor>().cursorTransform();
        _target    = _cursor.GetComponent<Workspace_Cursor>().cursorTarget();

        //setLayer();

    }


    /// <summary>
    /// Creates layer parenting
    /// </summary>
    private void setLayer()
    {
        // Gets curent layer from Grid
        GameObject temp = GameObject.Find("Workspace_Manager");
        _currentLayer = (int)temp.GetComponent<Workspace_Grid>().currentY + 1;

        // Creates a temp string for name checking / adding
        string newName = "Layer" + _currentLayer.ToString();

        // Checks if layer exsists, if so sets to that layer, else creates new layer
        bool addLayer = true;
        foreach(GameObject go in _parent)
        {
            // If layer Exsists
            if(go.name == newName)
            {
                addLayer = false;
                _layer = go;
            }
        }
        if(addLayer)
        {
            // new layer
            _layer = new GameObject();
            _layer.name = newName;
            _parent.Add(_layer);
        }
    }

    /// <summary>
    /// Create a block of type and color selected, at the position of the cursor
    /// </summary>
    public void createBlock()
    {
        _block = (GameObject)Instantiate(Resources.Load(_blockName), _cursorPos.transform.position, _cursorPos.transform.rotation);
        _block.name = _blockName;
        _block.GetComponent<Renderer>().material.color = ColorSelector.GetColor(Workspace_Manager.Instance.GetActiveBlockColor());
        _block.transform.parent = _layer.transform;
        _blockList.Add(_block);
        updateSceneScale();
    }

    /// <summary>
    /// Delete block currently selected by cursor
    /// </summary>
    public void deleteBlock()
    {
        _blockList.Remove(_target.gameObject);
        Destroy(_target.gameObject);
        updateSceneScale();
    }

    /// <summary>
    /// Updates the largest x position and z position within scene, for use by grid scaler
    /// </summary>
    private void updateSceneScale()
    {
        // Check Scale
        Vector2 tempScale = new Vector2(0.0f, 0.0f);
        foreach (GameObject go in _blockList)
        {
            // X Scale Check
            if (go.transform.position.x > tempScale.x)
            {
                tempScale.x = go.transform.position.x;
            }

            // Z Scale Check
            if (go.transform.position.z > tempScale.y)
            {
                tempScale.y = go.transform.position.z;
            }
        }
        // Update Scale
        _largestScale.x = tempScale.x;
        _largestScale.y = tempScale.y;

    }

    /// <summary>
    /// Returns Largest X coordinate
    /// </summary>
    /// <returns></returns>
    public float largestX()
    {
        return _largestScale.x;
    }

    /// <summary>
    /// Returns Largest Z Coordinate
    /// </summary>
    /// <returns></returns>
    public float largestZ()
    {
        return _largestScale.y;
    }

}
