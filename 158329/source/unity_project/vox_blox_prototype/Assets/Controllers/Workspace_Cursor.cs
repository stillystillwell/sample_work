﻿using UnityEngine;
using UnityStandardAssets.Characters.FirstPerson;

public class Workspace_Cursor : MonoBehaviour {

    private RaycastHit _target;
    private FirstPersonController _mouse;
    private GameObject _cBlock, _dBlock;
    private Transform _addBlock, _delBlock;
    private string _blockName;
    private int    _maxDistance = 20;
    private bool   _create, _edit;

    private Workspace_Block_Placer _blockPlacer;

    // Use this for initialization
    void Start()
    {
        // World
        _mouse       = GameObject.Find("FPS_Controller").GetComponent<FirstPersonController>();
        _blockPlacer = GameObject.Find("Workspace_Manager").GetComponent<Workspace_Block_Placer>();

        // Scene State Init
        _edit = false;
        _create = true;

        // Block Init
        _blockName  = "1001";

        // Cursor Init
        createCursor();

    }

    // Update is called once per frame
    void Update()
    {
        // Toggle block Add - Delete
        if (Input.GetKeyDown(KeyCode.LeftControl))
        {
            if (_edit) toggleCreate();
        }

        // Toggle between Menu - Edit
        if (Input.GetKeyDown(KeyCode.LeftShift))
        {
            toggleEdit();
            _mouse.ChangeCursorLock();

        }

        // Change Block rotation
        if (Input.GetKeyDown(KeyCode.Space))
        {
            if (_edit) rotateBlock();
        }

        // Move layer up
        if (Input.GetKeyDown(KeyCode.KeypadPlus))
        {
            if (_edit) Workspace_Manager.Instance.MoveWorkspaceLayerUp();
        }

        // Move layer down
        if (Input.GetKeyDown(KeyCode.KeypadMinus))
        {
            if (_edit) Workspace_Manager.Instance.MoveWorkspaceLayerDown();
        }

        // casts Ray into scene
        if (Physics.Raycast(Camera.main.ScreenPointToRay(new Vector3((Screen.width / 2), (Screen.height / 2), 0)), out _target, _maxDistance))
        {
            // Check to see workspace editing is enabled
            if (_edit)
            {
                // Creating Blocks
                if (_create)
                {

                    // Update Cursor Visual
                    _addBlock.GetComponent<Renderer>().enabled = true;
                    _delBlock.GetComponent<Renderer>().enabled = false;

                    // Update the position of the cursor
                    if (_target.transform.tag == "Floor")
                    {
                        Vector3 temp = _target.transform.position;
                        temp.x = (int)_target.point.x;
                        temp.z = (int)_target.point.z;
                        temp.y = Workspace_Manager.Instance.GetCurrentLayer();
                        _addBlock.transform.position = temp;
                        _delBlock.transform.position = temp;

                        _addBlock.GetComponent<VoxelBlock>().UpdatePostion();
                        _delBlock.GetComponent<VoxelBlock>().UpdatePostion();

                    }
                    else if (_target.transform.tag == "Block")
                    { 
                        Vector3 temp = _target.transform.position;
                        temp.x = (int)_target.point.x + _target.normal.x;
                        temp.z = (int)_target.point.z + _target.normal.z;
                        temp.y = Workspace_Manager.Instance.GetCurrentLayer();
                        _addBlock.transform.position = temp;
                        _delBlock.transform.position = temp;

                        _addBlock.GetComponent<VoxelBlock>().UpdatePostion();
                        //_delBlock.GetComponent<VoxelBlock>().UpdatePostion();

                    }

                    // Check if the requested block can be placed at this position
                    if (!Workspace_Manager.Instance.CanPlaceBlock(_addBlock))
                    {
                        // Block cannot be placed here - update the cursor
                        _addBlock.GetComponent<Renderer>().enabled = false;
                        changeCursorDelete(_addBlock.transform);
                        _delBlock.GetComponent<Renderer>().enabled = true;
                    }
                    else
                    {
                        // Block can be placed here - listen for mouse click
                        if (Input.GetMouseButtonDown(0))
                        {
                            Workspace_Manager.Instance.AddNewBlock(_addBlock.gameObject);
                        }

                    }

                }

                // Deleting Blocks
                if (!_create)
                {
                    // Update Cursor Visual
                    _addBlock.GetComponent<Renderer>().enabled = false;

                    if (_target.transform.tag == "Floor")
                    {
                        // Cursor not point at a block
                        Vector3 temp = _target.transform.position;
                        temp.x = (int)_target.point.x;
                        temp.z = (int)_target.point.z;
                        _delBlock.transform.position = temp;
                        _addBlock.transform.position = temp;
                        changeCursorDelete(_addBlock.transform);
                        _delBlock.GetComponent<Renderer>().enabled = false;

                    }
                    else if (_target.transform.tag == "Block")
                    {
                        
                        _delBlock.transform.position = _target.transform.position;
                        _delBlock.transform.rotation = _target.transform.rotation;
                        changeCursorDelete(_target.transform);
                        _delBlock.GetComponent<Renderer>().enabled = true;

                        if (Input.GetMouseButtonDown(0))
                        {
                            Workspace_Manager.Instance.DeleteBlock(_target.transform.gameObject);
                        }

                    }

                }

            }
            else
            {
                // Not in edit mode
                // Hide cursors
                _addBlock.GetComponent<Renderer>().enabled = false;
                _delBlock.GetComponent<Renderer>().enabled = false;
            }

        }

    }


    /// <summary>
    /// Returns the currently cursors block type
    /// </summary>
    /// <returns></returns>
    public string blockName()
    {
        return _blockName;
    }

    /// <summary>
    /// Returns the Cursors current position for creation of blocks
    /// </summary>
    /// <returns></returns>
    public Transform cursorTransform()
    {
        return _addBlock;
    }

    /// <summary>
    /// Returns the block the cursor is hitting
    /// </summary>
    /// <returns></returns>
    public Transform cursorTarget()
    {
        return _target.transform;
    }

    /// <summary>
    /// Returns the mode "Create Block" or "Delete Block"
    /// </summary>
    /// <returns></returns>
    public bool IsCreateMode()
    {
        return _create;
    }

    /// <summary>
    /// Returns the mode "Menu" or "Scence Edit"
    /// </summary>
    /// <returns></returns>
    public bool IsEditMode()
    {
        return _edit;
    }

    /// <summary>
    /// Changes the Block type to be placed 
    /// String
    /// </summary>
    /// <param name="b"></param> The new Block - type Object
    public void changeBlock(string name)
    {
        _blockName = name;
        changeCursor(_addBlock, _delBlock);
    }

    /// <summary>
    /// Toggles creation and deletion
    /// </summary>
    public void toggleCreate()
    {
        _create = !_create;
    }

    /// <summary>
    ///  Toggles Edit (menu/workspace)
    /// </summary>
    private void toggleEdit()
    {
        _edit = !_edit;
    }

    /// <summary>
    /// Instantiates The Cursor
    /// </summary>
    private void createCursor()
    {
        GameObject firstCursor = (GameObject)Instantiate(Resources.Load(_blockName), new Vector3(35.0f, -5.0f, 35.0f), Quaternion.identity);
        changeCursor(firstCursor.transform, firstCursor.transform);
        Destroy(firstCursor);
    }

    /// <summary>
    /// Changes the Cursor size and keeps old cursors transforms
    /// </summary>
    /// <param name="tc"></param>
    /// <param name="td"></param>
    private void changeCursor(Transform tc, Transform td)
    {
        Destroy(_cBlock);
        _cBlock = (GameObject)Instantiate(Resources.Load(_blockName), tc.position, tc.rotation);
        _cBlock.GetComponent<Renderer>().material.color = new Color(0.2f, 0.8f, 0.2f, 0.5f);
        _cBlock.GetComponent<Renderer>().material.SetInt("_SrcBlend", (int)UnityEngine.Rendering.BlendMode.One);
        _cBlock.GetComponent<Renderer>().material.SetInt("_DstBlend", (int)UnityEngine.Rendering.BlendMode.OneMinusSrcAlpha);
        _cBlock.GetComponent<Renderer>().material.SetInt("_ZWrite", 0);
        _cBlock.GetComponent<Renderer>().material.DisableKeyword("_ALPHATEST_ON");
        _cBlock.GetComponent<Renderer>().material.DisableKeyword("_ALPHABLEND_ON");
        _cBlock.GetComponent<Renderer>().material.EnableKeyword("_ALPHAPREMULTIPLY_ON");
        _cBlock.GetComponent<Renderer>().material.renderQueue = 3000;
        _cBlock.GetComponent<Collider>().enabled = false;
        _cBlock.transform.tag = "Untagged";
        _addBlock = _cBlock.transform;
        _addBlock.transform.parent = GameObject.Find("Cursors").transform;

        Destroy(_dBlock);
        _dBlock = (GameObject)Instantiate(Resources.Load(_blockName), td.position, td.rotation);
        _dBlock.GetComponent<Renderer>().material.color = new Color(0.8f, 0.2f, 0.2f, 0.7f);
        _dBlock.GetComponent<Renderer>().material.SetInt("_SrcBlend", (int)UnityEngine.Rendering.BlendMode.One);
        _dBlock.GetComponent<Renderer>().material.SetInt("_DstBlend", (int)UnityEngine.Rendering.BlendMode.OneMinusSrcAlpha);
        _dBlock.GetComponent<Renderer>().material.SetInt("_ZWrite", 0);
        _dBlock.GetComponent<Renderer>().material.DisableKeyword("_ALPHATEST_ON");
        _dBlock.GetComponent<Renderer>().material.DisableKeyword("_ALPHABLEND_ON");
        _dBlock.GetComponent<Renderer>().material.EnableKeyword("_ALPHAPREMULTIPLY_ON");
        _dBlock.GetComponent<Renderer>().material.renderQueue = 3000;
        _dBlock.GetComponent<Collider>().enabled = false;
        _dBlock.transform.tag = "Untagged";
        _delBlock = _dBlock.transform;
        _delBlock.transform.parent = GameObject.Find("Cursors").transform;
    }

    /// <summary>
    /// Changes the delete cursor based on the block hovered over
    /// </summary>
    /// <param name="blockName"></param>
    private void changeCursorDelete(Transform td)
    {
        GameObject tempGO = _dBlock;
        _dBlock = (GameObject)Instantiate(Resources.Load(td.GetComponent<VoxelBlock>().GetID().ToString()), _delBlock.transform.position, _delBlock.transform.rotation);
        _dBlock.GetComponent<Renderer>().material.color = new Color(0.8f, 0.2f, 0.2f, 0.7f);
        _dBlock.GetComponent<Renderer>().material.SetInt("_SrcBlend", (int)UnityEngine.Rendering.BlendMode.One);
        _dBlock.GetComponent<Renderer>().material.SetInt("_DstBlend", (int)UnityEngine.Rendering.BlendMode.OneMinusSrcAlpha);
        _dBlock.GetComponent<Renderer>().material.SetInt("_ZWrite", 0);
        _dBlock.GetComponent<Renderer>().material.DisableKeyword("_ALPHATEST_ON");
        _dBlock.GetComponent<Renderer>().material.DisableKeyword("_ALPHABLEND_ON");
        _dBlock.GetComponent<Renderer>().material.EnableKeyword("_ALPHAPREMULTIPLY_ON");
        _dBlock.GetComponent<Renderer>().material.renderQueue = 3000;
        _dBlock.GetComponent<Collider>().enabled = false;
        _dBlock.transform.tag = "Untagged";
        _delBlock = _dBlock.transform;
        _delBlock.GetComponent<Renderer>().enabled = true;
        _delBlock.transform.parent = GameObject.Find("Cursors").transform;
        Destroy(tempGO);

    }

    /// <summary>
    /// Rotates Cursor
    /// </summary>
    private void rotateBlock()
    {
        _addBlock.transform.Rotate(Vector3.up, 90.0f);
        _delBlock.transform.Rotate(Vector3.up, 90.0f);

        _addBlock.GetComponent<VoxelBlock>().UpdateRotation();
        _delBlock.GetComponent<VoxelBlock>().UpdateRotation();
    }
}
